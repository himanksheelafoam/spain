package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.SubmitInnerData
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility

import kotlinx.android.synthetic.main.fragment_submitdetail.view.*

import java.util.concurrent.TimeUnit

class SubmitDetailFragment : BaseFragment() {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    lateinit var submitresponse: SubmitInnerData
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_submitdetail, container, false)
        if (arguments != null) {

                   submitresponse = arguments?.getSerializable(Constants.SUBMIT) as SubmitInnerData
          if (submitresponse!=null){
              mView?.customer_name?.text=submitresponse?.customer?.name?.toString()
              if (submitresponse?.date?.date!=null) {
                  mView?.date?.text = Utility.convertDateFormat(submitresponse?.date?.date, "dd-MM-yyyy")
              }
              mView?.docket_no?.text=submitresponse?.appPlNumber?.toString()

          }

        }
        RxView.clicks(mView?.home).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
               Utility.replaceFragment(controllerActivity,DashBoardFragment(),null)
            }
        controllerActivity.hideKeyboard()



        return mView.rootView
    }
    override fun onbackListen() {
        super.onbackListen()
        controllerActivity.clearAllFragments()
        Utility.replaceFragment(controllerActivity, DashBoardFragment(), null)
    }

    override fun onResume() {
        super.onResume()

    }
    }