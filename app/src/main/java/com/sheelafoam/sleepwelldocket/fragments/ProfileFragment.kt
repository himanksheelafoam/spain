package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.activities.LoginActivity
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.util.concurrent.TimeUnit

class ProfileFragment : BaseFragment() {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_profile, container, false)
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.name != null) {


            mView.fullname.text = AppPrefrences.Data.getInstance(controllerActivity)
                    .getUserModel(controllerActivity, "userData")?.user?.name
        }
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.designation != null) {


            mView.designation.text = AppPrefrences.Data.getInstance(controllerActivity)
                    .getUserModel(controllerActivity, "userData")?.user?.designation
        }
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.phone != null) {


            mView.phone_number.text = AppPrefrences.Data.getInstance(controllerActivity)
                    .getUserModel(controllerActivity, "userData")?.user?.phone
        }
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.email != null) {


            mView.email.text = AppPrefrences.Data.getInstance(controllerActivity)
                    .getUserModel(controllerActivity, "userData")?.user?.email
        }
        val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.blob)
                .error(R.drawable.blob)
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.image != null) {

            Glide.with(controllerActivity.applicationContext).load(
                    AppPrefrences.Data.getInstance(controllerActivity).getUserModel(
                            controllerActivity,
                            "userData"
                    )?.profileImageUrl + "/" + AppPrefrences.Data.getInstance(controllerActivity)
                            .getUserModel(controllerActivity, "userData")?.user?.image!!
            ).apply(options).into(mView.profile_image)
        }
        RxView.clicks(mView.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    controllerActivity.onBackPressed()
                }
        RxView.clicks(mView.logout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    val editor = AppPrefrences.Data.getInstance(controllerActivity!!).clearPreference()
                    controllerActivity.finishAffinity()

                    val i = Intent(controllerActivity, LoginActivity::class.java)
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(i)
                }


        return mView.rootView
    }


    override fun onResume() {
        super.onResume()


    }


}