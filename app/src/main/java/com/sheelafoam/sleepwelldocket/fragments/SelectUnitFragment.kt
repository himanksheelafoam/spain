package com.sheelafoam.sleepwelldocket.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.UnitAdapter
import com.sheelafoam.sleepwelldocket.models.response.CustomerListData
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.models.response.UnitResponseModel
import com.sheelafoam.sleepwelldocket.models.response.Unitdata
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Predicate
import kotlinx.android.synthetic.main.fragment_select_unit.view.*
import java.util.concurrent.TimeUnit
import io.reactivex.functions.Consumer


class SelectUnitFragment : BaseFragment(), RetrofitListener,CommonItemClickListener {
    private var activity: Context? = null
    private var searchRequest: SearchRequest? = null
    var dashBoardFragment: DashBoardFragment?=null
    var unitData: Unitdata? = null
    var unitListData: ArrayList<UnitListData>? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_unit, container, false)
         searchRequest = SearchRequest()
        mView?.search_view.setHint("Search units")
        if (arguments != null) {
            unitData =
                arguments?.getSerializable(Constants.UNITS) as Unitdata?
            if (unitData!=null){
                unitListData= unitData?.data as ArrayList<UnitListData>?
                setAdapter(unitListData!!)
            }
        }

        mView.search_view.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                filter(s.toString())

            }

            override fun afterTextChanged(s: Editable) {

            }
        })



        return mView.rootView
    }
    fun filter(text: String) {
        val invitedPeople = ArrayList<UnitListData>()

        for (e in unitListData?.indices!!) {
            if (unitListData?.get(e)?.name?.toLowerCase()?.contains(text.toLowerCase())!!) {
                invitedPeople.add(unitListData?.get(e)!!)
            }
        }

        setAdapter(invitedPeople)


    }


    private fun callapi(keyword: String) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(UnitResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.UNITS)
        retrofit.setCall(retrofit.getApiInterface().getunits(keyword))

    }


    override fun onResume(){
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.UNITS, ignoreCase = true)) {
            hideProgress()
            response as UnitResponseModel
            if (response?.data!=null){
                setAdapter(response?.data?.data!!)
            }


        }
    }

    private fun setAdapter(data: List<UnitListData>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false)
        mView?.recycler?.adapter = UnitAdapter( data!!,this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity,responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is UnitListData) {
            dashBoardFragment = controllerActivity!!.supportFragmentManager.findFragmentByTag(DashBoardFragment::class.java.getName()) as DashBoardFragment?
            dashBoardFragment?.setunit(o!!)
        }
    }


}

