package com.sheelafoam.sleepwelldocket.fragments


import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.ToneGenerator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.BundleAdapter
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.CustomDialog
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_addproduct.view.*
import kotlinx.android.synthetic.main.fragment_batchscan.*
import kotlinx.android.synthetic.main.fragment_batchscan.view.*
import kotlinx.android.synthetic.main.fragment_batchscan.view.add
import kotlinx.android.synthetic.main.fragment_batchscan.view.back
import kotlinx.android.synthetic.main.fragment_batchscan.view.barcode
import kotlinx.android.synthetic.main.fragment_batchscan.view.recycler
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.*
import kotlinx.android.synthetic.main.invalid_popup.*
import kotlinx.android.synthetic.main.invalid_popup.message
import kotlinx.android.synthetic.main.success_popup.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class BatchScanFragment : BaseFragment(),CommonItemClickListener {
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    private var barcodeDetect: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201
    private var toneGen1: ToneGenerator? = null
    var dialog: CustomDialog? = null
    var barcoderesponse:Boolean=true

    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String = ""
    var customername: String = ""
    lateinit var bundles: ArrayList<String>

     override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_batchscan, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            customername = arguments?.getString(Constants.CUSTOMERS)!!

            if ( arguments?.getSerializable(Constants.BUNDLE)!=null){
                bundles =
                        (arguments?.getSerializable(Constants.BUNDLE) as ArrayList<String>?)!!
                setAdapter(bundles)
            }else{
                bundles = ArrayList<String>()

            }



        }
        controllerActivity.hideKeyboard()

        mView?.barcode.setQueryHint(getString(R.string.docket_number))
        mView?.barcode.onActionViewExpanded()
        mView?.barcode.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(s: String): Boolean {
               if (s.length==14){

                   controllerActivity.hideKeyboard()
                   if (isValidate()) {
                       if (!bundles.contains(s.trim())) {
                           bundles.add(s)
                           setAdapter(bundles)
                           mView?.barcode.setQuery("", false)
                           val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                           toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)

                           //  opensuccesspopup(s)
                           mView?.entries.text = bundles.size.toString() + " " + getString(R.string.bundles)


                       } else {
                           openerrorpopup(s)
                           mView?.barcode.setQuery("", false)

                       }
                   }
               }
                return true
            }

            override fun onQueryTextSubmit(s: String): Boolean {

                controllerActivity.hideKeyboard()
                if (isValidate()) {
                    if (!bundles.contains(s.trim())) {
                        bundles.add(s)
                        setAdapter(bundles)
                        mView?.barcode.setQuery("", false)
                        val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)

                        //  opensuccesspopup(s)
                        mView?.entries.text = bundles.size.toString() + " " + getString(R.string.bundles)


                    } else {
                        openerrorpopup(s)
                        mView?.barcode.setQuery("", false)

                    }
                }
                return true
            }
        })

        RxView.clicks(mView?.add!!).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
               if (isValidate()) {
                   if (!bundles.contains(mView?.barcode.query.toString().trim())) {
                       bundles.add(mView?.barcode.query.toString())
                       setAdapter(bundles)
                       val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                       toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)

                       //  opensuccesspopup(mView?.barcode.query.toString())
                        mView?.entries.text =
                           bundles.size.toString() + " " + getString(R.string.bundles)
                       mView?.barcode.setQuery("",false)
                       controllerActivity.hideKeyboard()


                   } else {
                       openerrorpopup(mView?.barcode.query.toString())
                       mView?.barcode.setQuery("",false)
                       controllerActivity.hideKeyboard()

                   }
               }
            }
        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        RxView.clicks(mView?.validate).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                if (!bundles.isEmpty()) {
                    val bundle = Bundle()

                    bundle.putInt(Constants.UNIT_ID, unitId)
                    bundle.putString(Constants.UNITS, unitname)
                    bundle.putInt(Constants.CUSTOMER_ID, customerId)
                    bundle.putString(Constants.CUSTOMERS, customername)
                    bundle.putSerializable(Constants.BUNDLE, bundles)

                    Utility.addFragment(
                        controllerActivity,
                        AfterValidationDispatchFragment(),
                        bundle
                    )
                }else{
                    Utility.errorToast(controllerActivity,getString(R.string.please_add_bundle))
                }
            }
        toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)



        return mView.rootView
    }

    private fun isValidate(): Boolean {
        if (mView?.barcode?.query.toString().isEmpty()){
            Utility.errorToast(controllerActivity,getString(R.string.enter_the_bundle))
            return false
        }else if (mView?.barcode?.query.toString().length>14){
            Utility.errorToast(controllerActivity, getString(R.string.bundle_should_less_than))
            return false
        }
        return true

    }

    override fun onPause() {
        super.onPause()
        cameraSource?.release()

    }

    override fun onResume() {
        super.onResume()
        if (Utility.checkAndRequestGalleryPermissions(controllerActivity)) {
            initialiseDetectorsAndSources()

        }
        controllerActivity.hideKeyboard()



    }
//    2560x1440

    private fun initialiseDetectorsAndSources() {

        barcodeDetect = BarcodeDetector.Builder(controllerActivity)
            .setBarcodeFormats(Barcode.ALL_FORMATS)
            .build()
        cameraSource = CameraSource.Builder(controllerActivity, barcodeDetect)
            .setRequestedPreviewSize(2560, 1440)
            .setRequestedFps(15.0f)
            .setAutoFocusEnabled(true)
            .build()

        surfaceview?.getHolder()?.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    cameraSource?.start(surfaceview?.getHolder())

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }


            override fun surfaceChanged(
                holder: SurfaceHolder,
                format: Int,
                width: Int,
                height: Int
            ) {
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource?.stop()
            }
        })



        barcodeDetect?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                // Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() != 0) {
                    val model = barcodes.valueAt(0).displayValue as String
                    if (!model.equals("")) {
                        if (barcoderesponse) {
                            barcoderesponse = false
                            toneGen1?.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                            if (activity != null) {
                                //barcodeDetect?.release()
                                  Thread.sleep(2000)

                                controllerActivity.runOnUiThread(Runnable {
                                    if (!bundles.contains(model)) {
                                        bundles.add(model)
                                        setAdapter(bundles)
                                        opensuccesspopup(model)
                                        mView?.entries.text = bundles.size.toString() + " bundles"


                                    } else {
                                        openerrorpopup(model)

                                    }
                                })
                            }
                        }
                    }
                }
            }
        })

    }

    private fun openerrorpopup(model: String) {
        hideProgress()
        controllerActivity.hideKeyboard()

        val mp: MediaPlayer = MediaPlayer.create(controllerActivity, R.raw.error_tone) // sound is inside res/raw/mysound

        dialog = CustomDialog(getActivity(), R.layout.invalid_popup)
        val window = dialog!!.window
        window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        if (Utility.checkAndRequestGalleryPermissions(controllerActivity)) {
            initialiseDetectorsAndSources()

        }
        dialog?.cancelok?.setOnClickListener(View.OnClickListener {
            dialog?.dismiss()
            barcoderesponse=true
            mp.stop()

        })
          dialog?.message?.text=getString(R.string.bundle_number)+" "+model+" "+getString(R.string.duplicate_bundle_found)
            mp.start()
        dialog!!.show()
    }
    private fun opensuccesspopup(model: String) {
        hideProgress()
        dialog = CustomDialog(getActivity(), R.layout.success_popup)
        val window = dialog!!.window
        window!!.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.successmessage?.text=getString(R.string.bundle_number)+" "+model+" "+getString(R.string.added_successfully)
        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog?.ok?.setOnClickListener(View.OnClickListener {
           dialog?.dismiss()
           barcoderesponse=true

         })
        val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
        toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)



        dialog!!.show()
    }


    private fun setAdapter(bundle: ArrayList<String>) {


        mView?.recycler?.layoutManager = LinearLayoutManager(
            getActivity(),
            LinearLayoutManager.VERTICAL,
            false
        )
        mView?.recycler?.adapter = BundleAdapter(bundle!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }




    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is String) {

            bundles?.remove(o)
            setAdapter(bundles)
            mView?.entries.text=bundles.size.toString() +" "+getString(R.string.bundles)
        }
    }


}