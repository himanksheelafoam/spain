package com.sheelafoam.sleepwelldocket.fragments


import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.ToneGenerator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.SurfaceHolder
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.SearchView
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.barcode.Barcode
import com.google.android.gms.vision.barcode.BarcodeDetector
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.models.request.SaveDraftRequestModel
import com.sheelafoam.sleepwelldocket.models.request.ScanDetailsRequestModel
import com.sheelafoam.sleepwelldocket.models.request.ScanRequestModel
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.SaveDraftResponseModel
import com.sheelafoam.sleepwelldocket.models.response.ScanDetailResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.CustomDialog
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_addproduct.*
import kotlinx.android.synthetic.main.fragment_addproduct.view.*
import kotlinx.android.synthetic.main.invalid_popup.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class AddProductFragment : BaseFragment(), RetrofitListener {
    private var barcodeDetector: BarcodeDetector? = null
    private var cameraSource: CameraSource? = null
    private val REQUEST_CAMERA_PERMISSION = 201
    private var toneGen1: ToneGenerator? = null
    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String? = null
    var type: String? = null
    var from: String? = null
    var customername: String ?=null
    var dialog: CustomDialog? = null
    var barcoderesponse: Boolean = true
    lateinit var bundleListData: BundleListData
    lateinit var bundleList: ArrayList<BundleListData>
    lateinit var scanmodel: ScanRequestModel
    lateinit var model: SaveDraftRequestModel
    lateinit var bundle: ArrayList<String>
    lateinit var previousbundlelist: ArrayList<String>
    lateinit var bundlelist: ArrayList<BundleListData>
    var dispatchDocketFragment: DispatchDocketFragment?=null
    var count:Int=0
    var referencenumber=""
    private var controllerActivity: ControllerActivity = ControllerActivity()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_addproduct, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            customername = arguments?.getString(Constants.CUSTOMERS)!!
            type = arguments?.getString(Constants.ACTIONTYPE)
            from = arguments?.getString(Constants.FROM)
            referencenumber= arguments?.getString(Constants.REFERENCE)!!

            bundlelist = ArrayList<BundleListData>()
            previousbundlelist = ArrayList<String>()
            view?.barcode?.requestFocus()
            controllerActivity.hideKeyboard()


            view?.unit?.text = unitname
            view?.customer_name?.text = customername
            if (type != null) {
                view?.heading?.setText(type)
                 if (type.equals(getString(R.string.add_product))) {
                     view?.add_text?.setText(getString(R.string.add))
                 }else if (type.equals(getString(R.string.remove_Product))){
                     view?.add_text?.setText(getString(R.string.remove))

                 }
            }

            if (arguments?.getSerializable(Constants.SCANDETAILS) != null) {
                bundleList = (arguments?.getSerializable(Constants.SCANDETAILS) as ArrayList<BundleListData>?)!!
                bundlelist = ArrayList<BundleListData>()
                count=bundleList.size
                view.count.setText(count.toString())

                if (from.equals(getString(R.string.cart))){
                    if (bundleList != null && !bundleList.isEmpty()) {
                        showProgress(controllerActivity)

                        for (i in bundleList?.indices) {
                            bundlelist.add(bundleList?.get(i))
                            bundleList.get(i).prodBulto?.let { previousbundlelist.add(it)
                           }

                        }
                        hideProgress()

                    }
                }else {


                    if (bundleList != null && !bundleList.isEmpty()) {
                        for (i in bundleList!!.indices) {
                            bundlelist.add(bundleList!!.get(i))
                            bundleList.get(i).ownbundle_number?.let { previousbundlelist.add(it) }

                        }

                    }
                }
            } else {
                bundleList = ArrayList<BundleListData>()
            }


        }
         scanmodel = ScanRequestModel()
        bundle = ArrayList<String>()
        view.barcode.onActionViewExpanded()
        view.barcode.setQueryHint(getString(R.string.docket_number))


        view.barcode.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(s: String): Boolean {
                // make a server call
                if (type.equals(getString(R.string.add_product))) {
                    if (s.length == 14) {
                        bundle.clear()


                        controllerActivity.hideKeyboard()


                        if (previousbundlelist.size > 0) {

                            if (!previousbundlelist.contains(s.trim())!!) {
                                bundle.add(s)
                                scanmodel.unit_id = unitId
                                scanmodel.customer_id = customerId
                                scanmodel.bundles = s
                                callscanapi(scanmodel)
                            } else {
                                openerrorpopup(s, getString(R.string.duplicate_bundle_found))
                                view.barcode.setQuery("", false)
                            }
                        } else {
                            bundle.add(view.barcode.toString())
                            scanmodel.unit_id = unitId
                            scanmodel.customer_id = customerId
                            scanmodel.bundles = s
                            callscanapi(scanmodel)
                        }
                        view.barcode.setQuery("", false)

                    }
                } else if (type.equals(getString(R.string.remove_Product))) {
                    if (s.length == 14) {
                        bundle.clear()


                        controllerActivity.hideKeyboard()
                        view.barcode.setQuery("", false)


                        if (previousbundlelist.size > 0) {

                            if (previousbundlelist.contains(s.trim())!!) {
                                for (i in bundlelist.indices) {

                                    if (i >= bundlelist.size) {
                                        // Warning, invalid position
                                    } else {
                                        if (bundlelist.get(i).ownbundle_number.equals(s.trim())) {
                                            bundlelist.removeAt(i)
                                            if (i >= previousbundlelist.size) {
                                                // Warning, invalid position
                                            }else {
                                                previousbundlelist.removeAt(i)
                                            }
                                            Utility.showToast(controllerActivity,getString(R.string.removed_successfully))
                                        }


                                    }
                                }
                            }else{
                                Utility.errorToast(controllerActivity,getString(R.string.bundle_not_found))

                            }
                        }

                    }
                }
                return true
            }

            override fun onQueryTextSubmit(s: String): Boolean {
                bundle.clear()


                controllerActivity.hideKeyboard()

                if (isValidate()) {
                    if (type.equals(getString(R.string.add_product))) {

                        if (previousbundlelist.size > 0) {

                            if (!previousbundlelist.contains(
                                            s.trim())!!
                            ) {
                                bundle.add(s.toString())
                                scanmodel.unit_id = unitId
                                scanmodel.customer_id = customerId
                                scanmodel.bundles = s.toString()
                                callscanapi(scanmodel)
                            } else {
                                openerrorpopup(
                                        s.toString(),
                                        getString(R.string.duplicate_bundle_found)
                                )
                                view.barcode.setQuery("", false)
                            }
                        } else {
                            bundle.add(view.barcode.toString())
                            scanmodel.unit_id = unitId
                            scanmodel.customer_id = customerId
                            scanmodel.bundles = s.toString()
                            callscanapi(scanmodel)
                        }
                    } else if (type.equals(getString(R.string.remove_Product))) {
                        if (previousbundlelist.size > 0) {

                            if (previousbundlelist.contains(s.trim())!!) {
                                for (i in bundlelist.indices) {

                                    if (i >= bundlelist.size) {
                                        // Warning, invalid position
                                    } else {
                                        if (bundlelist.get(i).ownbundle_number?.equals(s.trim())!!) {
                                            bundlelist.removeAt(i)
                                            if (i >= previousbundlelist.size) {
                                                // Warning, invalid position
                                            }else {
                                                previousbundlelist.removeAt(i)
                                            }
                                            Utility.showToast(controllerActivity,getString(R.string.removed_successfully))

                                        }


                                    }
                                }
                            }else{
                                Utility.errorToast(controllerActivity,getString(R.string.bundle_not_found))

                            }

                        }
                        view.barcode.setQuery("", false)

                    }
                }
                return true
            }
        })
        RxView.clicks(view?.add!!).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    bundle.clear()

                    controllerActivity.hideKeyboard()
                    if (isValidate()) {
                        if (type.equals(getString(R.string.add_product))) {

                            if (previousbundlelist.size > 0) {

                                if (!previousbundlelist.contains(
                                                view.barcode.query.toString().trim())!!
                                ) {
                                    bundle.add(view.barcode.query.toString())
                                    scanmodel.unit_id = unitId
                                    scanmodel.customer_id = customerId
                                    scanmodel.bundles = view.barcode.query.toString()
                                    callscanapi(scanmodel)
                                } else {
                                    openerrorpopup(
                                            view.barcode.query.toString(),
                                            getString(R.string.duplicate_bundle_found)
                                    )
                                    view.barcode.setQuery("", false)
                                }
                            } else {
                                bundle.add(view.barcode.toString())
                                scanmodel.unit_id = unitId
                                scanmodel.customer_id = customerId
                                scanmodel.bundles = view.barcode.query.toString()
                                callscanapi(scanmodel)
                            }
                        } else if (type.equals(getString(R.string.remove_Product))) {
                            if (previousbundlelist.size > 0) {

                                if (previousbundlelist.contains(view.barcode.query.toString().trim())!!) {
                                    for (i in bundlelist.indices) {

                                        if (i >= bundlelist.size) {
                                            // Warning, invalid position
                                        } else {
                                            if (bundlelist.get(i).ownbundle_number?.equals(view.barcode.query.toString().trim())!!) {
                                                bundlelist.removeAt(i)
                                                if (i >= previousbundlelist.size) {
                                                    // Warning, invalid position
                                                }else {
                                                    previousbundlelist.removeAt(i)
                                                }
                                                Utility.showToast(controllerActivity,getString(R.string.removed_successfully))

                                            }


                                        }
                                    }
                                }else{
                                    Utility.errorToast(controllerActivity,getString(R.string.bundle_not_found))

                                }

                            }
                            view.barcode.setQuery("", false)

                        }
                    }

                }
        RxView.clicks(view.back!!).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    controllerActivity.onBackPressed()
                }
        RxView.clicks(view.batch_scan!!).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    val bundle = Bundle()
                    bundle.putInt(Constants.UNIT_ID, unitId)
                    bundle.putString(Constants.UNITS, view?.unit!!.text.toString())
                    bundle.putInt(Constants.CUSTOMER_ID, customerId)
                    bundle.putString(Constants.CUSTOMERS, view?.customer_name!!.text.toString())
                    Utility.addFragment(controllerActivity, BatchScanFragment(), bundle)

                }
        RxView.clicks(view.save_asdraft!!).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    if (!previousbundlelist.isEmpty()) {
                        model = SaveDraftRequestModel()
                        model.unit_id = unitId
                        model.customer_id = customerId
                        model.bundles = previousbundlelist
                        model.reference_number=referencenumber

                        callsavedraftapi(model)
                    }else{
                        Utility.errorToast(controllerActivity,getString(R.string.enter_the_bundle))
                    }
                }
        RxView.clicks(view?.done).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    var bundle = Bundle()
                    bundle.putInt(Constants.UNIT_ID, unitId)
                    bundle.putString(Constants.UNITS, view?.unit!!.text.toString())
                    bundle.putInt(Constants.CUSTOMER_ID, customerId)
                    bundle.putString(Constants.CUSTOMERS, view?.customer_name!!.text.toString())
                    bundle.putSerializable(Constants.SCANDETAILS, bundlelist)
                    bundle.putSerializable(Constants.BUNDLE, previousbundlelist)
                    bundle.putString(Constants.REFERENCE,referencenumber)


                    Utility.addFragment(controllerActivity, DispatchDocketProductFragment(), bundle)

                }

        toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)


        return view.rootView
    }

    private fun callsavedraftapi(model: SaveDraftRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(SaveDraftResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.SAVEDRAFT)
        retrofit.setCall(retrofit.getApiInterface().saveasdraft(model))

    }

    private fun isValidate(): Boolean {
        if (view?.barcode?.query.toString().isEmpty()) {
            Utility.errorToast(controllerActivity, getString(R.string.enter_the_bundle))
            return false
        } else if (view?.barcode?.query.toString().length > 14) {
            Utility.errorToast(controllerActivity, getString(R.string.bundle_should_less_than))
            return false
        }
        return true

    }


    override fun onPause() {
        super.onPause()
        cameraSource?.release()

    }

    override fun onResume() {
        super.onResume()
        controllerActivity.hideKeyboard()

        if (Utility.checkAndRequestGalleryPermissions(controllerActivity)) {
            initialiseDetectorsAndSources()


        }


    }


    private fun initialiseDetectorsAndSources() {

        barcodeDetector = BarcodeDetector.Builder(controllerActivity)
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build()
        cameraSource = CameraSource.Builder(controllerActivity, barcodeDetector)
                .setRequestedPreviewSize(2560, 1440)
                .setRequestedFps(15.0f)
                .setAutoFocusEnabled(true)
                .build()

        surface?.getHolder()?.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceCreated(holder: SurfaceHolder) {
                try {
                    cameraSource?.start(surface?.getHolder())

                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }


            override fun surfaceChanged(
                    holder: SurfaceHolder,
                    format: Int,
                    width: Int,
                    height: Int
            ) {
            }

            override fun surfaceDestroyed(holder: SurfaceHolder) {
                cameraSource?.stop()
            }
        })



        barcodeDetector?.setProcessor(object : Detector.Processor<Barcode> {
            override fun release() {
                // Toast.makeText(getApplicationContext(), "To prevent memory leaks barcode scanner has been stopped", Toast.LENGTH_SHORT).show();
            }

            override fun receiveDetections(detections: Detector.Detections<Barcode>) {
                val barcodes = detections.detectedItems
                if (barcodes.size() != 0) {
                    var model = barcodes.valueAt(0).displayValue as String
                    if (!model.equals("")) {
                        if (barcoderesponse) {
                            barcoderesponse = false
                            toneGen1?.startTone(ToneGenerator.TONE_CDMA_PIP, 150)

                            if (activity != null) {
                                // barcodeDetector?.release()
                                Thread.sleep(1000)
                                controllerActivity.runOnUiThread(Runnable {
                                    bundle.add(model)
                                    scanmodel.unit_id = unitId
                                    scanmodel.customer_id = customerId
                                    scanmodel.bundles = model
                                    callscanapi(scanmodel)

                                })
                            }
                        }

                    }

                }
            }
        })
    }

    private fun callscanapi(scanmodel: ScanRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(ScanDetailResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.SCANDETAILS)
        retrofit.setCall(retrofit.getApiInterface().scandetails(scanmodel))

    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.SCANDETAILS, ignoreCase = true)) {
            hideProgress()
             response as ScanDetailResponseModel
            if (response.data != null) {
                view?.barcode?.setQuery("", false)


                if (response.data?.data != null) {
                    if (response.data?.data?.get(0)?.status == false) {
                        openerrorpopup(response.data?.data?.get(0)?.bundleNumber, getString(R.string.doesnot_exist))

                    } else {






                        if(bundlelist!=null&&bundlelist.size>0) {
                            if(response.data?.data?.get(0)?.bundledata?.delivery_address!=null) {
                                if (response.data?.data?.get(0)?.bundledata?.delivery_address?.equals(bundlelist.get(0).delivery_address)!!) {
                                    previousbundlelist.add(response?.data?.data?.get(0)?.bundleNumber.toString())
                                    bundleListData = response.data?.data?.get(0)?.bundledata!!
                                    bundleListData.ownbundle_number = response?.data?.data?.get(0)?.bundleNumber.toString()
                                    val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                                    toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                                    barcoderesponse = true
                                    bundlelist.add(bundleListData!!)
                                    controllerActivity.hideKeyboard()


                                } else {
                                    Utility.errorToast(controllerActivity, "We cannot add bundle with another delivery address")
                                }
                            }else{
                                previousbundlelist.add(response?.data?.data?.get(0)?.bundleNumber.toString())
                                bundleListData = response.data?.data?.get(0)?.bundledata!!
                                bundleListData.ownbundle_number = response?.data?.data?.get(0)?.bundleNumber.toString()
                                val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                                toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                                barcoderesponse = true
                                bundlelist.add(bundleListData!!)
                                controllerActivity.hideKeyboard()


                            }
                        } else {
                                previousbundlelist.add(response?.data?.data?.get(0)?.bundleNumber.toString())
                                bundleListData = response.data?.data?.get(0)?.bundledata!!
                                bundleListData.ownbundle_number = response?.data?.data?.get(0)?.bundleNumber.toString()
                                val toneGen1 = ToneGenerator(AudioManager.STREAM_MUSIC, 100)
                                toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150)
                                barcoderesponse = true
                                bundlelist.add(bundleListData!!)
                                controllerActivity.hideKeyboard()
                            }
                        count=bundlelist.size
                        view?.count?.setText(count.toString())

                    }
                }

            }
        }
        else   if (requestType.equals(Constants.SAVEDRAFT, ignoreCase = true)) {
            hideProgress()
            response as SaveDraftResponseModel
            if (response.data != null) {
            Utility.showToast(controllerActivity,response?.data?.message?.toString())
                bundlelist.clear()
                previousbundlelist.clear()
                Utility.replaceFragment(controllerActivity,DashBoardFragment(),null)
               /* dispatchDocketFragment = controllerActivity!!.supportFragmentManager.findFragmentByTag(DispatchDocketFragment::class.java.getName()) as DispatchDocketFragment?
                dispatchDocketFragment?.setback()*/

            }

            }


    }

    private fun openerrorpopup(bundle: String?, remark: String?) {
        // barcoderesponse=true
        controllerActivity.hideKeyboard()

        val mp: MediaPlayer = MediaPlayer.create(controllerActivity, R.raw.error_tone) // sound is inside res/raw/mysound

        dialog = CustomDialog(getActivity(), R.layout.invalid_popup)
        val window = dialog!!.window
        window!!.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        )

        dialog?.setCanceledOnTouchOutside(false)
        dialog?.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        dialog?.message?.text = getString(R.string.bundle_number) + " " + bundle + " " + remark.toString()
        dialog?.cancelok?.setOnClickListener(View.OnClickListener {
            dialog?.dismiss()
            barcoderesponse = true
            mp.stop()

        })
        mp.start()

        dialog!!.show()
    }


    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }


}