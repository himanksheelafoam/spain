package com.sheelafoam.sleepwelldocket.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.DocketProductAdapter
import com.sheelafoam.sleepwelldocket.adapter.ListDetailAdapter
import com.sheelafoam.sleepwelldocket.models.response.*
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_dispatch_detail.view.*
import kotlinx.android.synthetic.main.fragment_dispatch_detail.view.back
import kotlinx.android.synthetic.main.fragment_dispatch_detail.view.bundle
import kotlinx.android.synthetic.main.fragment_dispatch_detail.view.customer_name
import kotlinx.android.synthetic.main.fragment_dispatch_detail.view.recycler
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.*
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class DispatchDetailFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var m3c: Double = 0.00
    lateinit var dispatchdetail: DispatchListData

    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_dispatch_detail, container, false)

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    controllerActivity.onBackPressed()
                }
        controllerActivity.hideKeyboard()

        if (arguments != null) {

            dispatchdetail = arguments?.getSerializable(Constants.DISPATCHDETAIL) as DispatchListData
            if (dispatchdetail != null) {
                callApi(dispatchdetail.appPlNumber.toString())
                mView?.customer_name?.text=dispatchdetail?.cPIdName.toString()
                mView?.docket_no?.text=dispatchdetail?.appPlNumber.toString()
                mView?.date?.text=Utility.convertDateFormat(dispatchdetail?.appPlDate.toString(),"dd-MM-yyyy")

            }
        }



        return mView.rootView
    }

    private fun callApi(docketid: String) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(DispatchDetailResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.DISPATCHDETAIL)
        retrofit.setCall(retrofit.getApiInterface().getdispatchlistdetail(docketid))

    }


    override fun onResume() {
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.DISPATCHDETAIL, ignoreCase = true)) {
            hideProgress()
            m3c=0.00
            response as DispatchDetailResponseModel
            if (response.data != null) {
                if (response.data?.data != null) {
                    setAdapter(response?.data?.data!!)
                    for (i in response.data?.data?.indices!!) {
                        if (response.data?.data?.get(i)?.m3c!=null) {
                            m3c += response.data?.data?.get(i)?.m3c!!
                        }}
                    val df = DecimalFormat("#.##")
                    val dx: String = df.format(m3c)
                    mView?.totalm3c.setText(dx.toString())
                    mView?.bundle.setText(response.data?.data?.size?.toString())

                }
            }


        }else  if (requestType.equals(Constants.DELETEDISPATCH, ignoreCase = true)) {
            hideProgress()
            response as DeleteDispatchResponseModel
            Utility.showToast(controllerActivity,response?.data?.message.toString())
            if (dispatchdetail != null) {
                callApi(dispatchdetail.appPlNumber.toString())
            }
        }
        }

    private fun setAdapter(data: List<DispatchDetailListModel>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false
        )
        mView?.recycler?.adapter = DocketProductAdapter(data!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)


    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is DispatchDetailListModel) {
            callDeleteApi(o?.id)

        }

        }

    private fun callDeleteApi(id: Int?) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(DeleteDispatchResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.DELETEDISPATCH)
        retrofit.setCall(id?.let { retrofit.getApiInterface().deletedocket(it) })

    }


}