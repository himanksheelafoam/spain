package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.util.Log
import androidx.fragment.app.Fragment
import com.kaopiz.kprogresshud.KProgressHUD

open class BaseFragment : Fragment() {
    var hud: KProgressHUD? = null



    protected open fun showProgress(context: Context?): Unit {
        Log.d("Progress", "baseActiviry")
        if (context != null) {
            hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setAnimationSpeed(2)
            hud?.show()

        }
    }

    protected open fun hideProgress() {
        Log.d("Progress", "baseActiviry")
        hud?.dismiss()
    }
    open fun onbackListen() {}


}
