package com.sheelafoam.sleepwelldocket.fragments


import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.DocketProductAdapter
import com.sheelafoam.sleepwelldocket.models.request.ScanDetailsRequestModel
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.SubmitResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.CustomDialog
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.*
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.*
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit


class DispatchDocketProductFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var unitId: Int =0
    var customerId: Int =0
    var unitname: String?=null
    var customername: String?=null
    var bundlescount: Int = 0
    var m3c: Double = 0.00
    var quantity: Double = 0.00
    var referencenumber=""
    lateinit var scanmodel: ScanDetailsRequestModel
    lateinit var bundle: ArrayList<String>
    lateinit var bundleList: ArrayList<BundleListData>


    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_dispatchdocketproduct, container, false)
        controllerActivity.hideKeyboard()

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)
            customername = arguments?.getString(Constants.CUSTOMERS)
            referencenumber= arguments?.getString(Constants.REFERENCE).toString()
            mView?.unit?.text = unitname
            mView?.customer_name?.text = customername
             if ( arguments?.getSerializable(Constants.SCANDETAILS)!=null){
                bundleList =
                    (arguments?.getSerializable(Constants.SCANDETAILS) as ArrayList<BundleListData>?)!!

            }else{
                bundleList= ArrayList<BundleListData>()
            }
            bundle = ArrayList<String>()

            if (bundleList != null) {
                setAdapter(bundleList)

                for (i in bundleList?.indices) {
                    if (bundleList?.get(i)?.m3c!=null) {
                        m3c += bundleList?.get(i)?.m3c!!
                    }
                    if (bundleList?.get(i)?.quantityNos!=null) {
                        quantity += bundleList?.get(i)?.quantityNos!!
                    }
                    bundlescount = bundleList?.size!!
                    mView?.bundle?.text = bundlescount.toString()
                    bundle.add(bundleList?.get(i)?.bundleNumber!!)

                }
                val df = DecimalFormat("#.##")
                val dx: String = df.format(m3c)

                mView?.m3c?.text = dx.toString()
                val pc = DecimalFormat("#.##")
                val pcs: String = pc.format(quantity)

                mView?.totalpcs?.text = pcs.toString()


            }

        }

        RxView.clicks(mView?.add_product).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->

                if (unitId != 0) {
                    var bundle = Bundle()
                    bundle.putInt(Constants.UNIT_ID, unitId!!)
                    bundle.putString(Constants.UNITS, mView?.unit.text.toString())
                    bundle.putInt(Constants.CUSTOMER_ID, customerId!!)
                    bundle.putString(Constants.ACTIONTYPE, getString(R.string.add_product))
                    bundle.putString(Constants.CUSTOMERS, mView?.customer_name.text.toString())
                    bundle.putSerializable(Constants.SCANDETAILS, bundleList!!)
                     bundle.putString(Constants.FROM, getString(R.string.dispatch_docket))
                    bundle.putString(Constants.REFERENCE,referencenumber)

                    Utility.addFragment(controllerActivity, AddProductFragment(), bundle)
                }

            }
        RxView.clicks(mView?.remove_product).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->

                    if (unitId != 0) {
                        var bundle = Bundle()
                        bundle.putInt(Constants.UNIT_ID, unitId!!)
                        bundle.putString(Constants.UNITS, mView?.unit.text.toString())
                        bundle.putInt(Constants.CUSTOMER_ID, customerId!!)
                        bundle.putString(Constants.ACTIONTYPE, getString(R.string.remove_Product))
                        bundle.putString(Constants.CUSTOMERS, mView?.customer_name.text.toString())
                        bundle.putSerializable(Constants.SCANDETAILS, bundleList!!)
                        bundle.putString(Constants.FROM, getString(R.string.dispatch_docket))
                        bundle.putString(Constants.REFERENCE,referencenumber)


                        Utility.addFragment(controllerActivity, AddProductFragment(), bundle)
                    }

                }
        RxView.clicks(mView?.submit).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                scanmodel = ScanDetailsRequestModel()

                scanmodel.unit_id = unitId
                scanmodel.customer_id = customerId
                scanmodel.bundles = bundle
                scanmodel.referencenumber=referencenumber
                callApi(scanmodel)

            }


        return mView.rootView
    }

    private fun callApi(scanmodel: ScanDetailsRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(SubmitResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.SUBMIT)
        retrofit.setCall(retrofit.getApiInterface().submit(scanmodel))

    }

    private fun setAdapter(bundleListData: ArrayList<BundleListData>) {

        mView?.recycler?.layoutManager = LinearLayoutManager(
            getActivity(),
            LinearLayoutManager.VERTICAL,
            false
        )
        mView?.recycler?.adapter = DocketProductAdapter(bundleListData!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }


    override fun onResume() {
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.SUBMIT, ignoreCase = true)) {
            hideProgress()
            response as SubmitResponseModel
            var bundle = Bundle()
            bundle.putSerializable(Constants.SUBMIT, response.data?.data)
            Utility.addFragment(controllerActivity, SubmitDetailFragment(), bundle)
        }
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }
    override fun onbackListen() {
        super.onbackListen()
        controllerActivity.clearAllFragments()
        Utility.replaceFragment(controllerActivity, DashBoardFragment(), null)
    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is BundleListData) {
             opendialog(o)


        }

    }

    private fun opendialog(o: BundleListData) {

        AlertDialog.Builder(controllerActivity)
            .setTitle(getString(R.string.remove_Product))
            .setMessage(getString(R.string.are_yousure)) // Specifying a listener allows you to take an action before dismissing the dialog.
              .setPositiveButton(android.R.string.yes,
                { dialog, which ->
                    m3c=0.00
                    quantity=0.00

                    bundleList?.remove(o)
                    if (bundleList!=null) {
                        setAdapter(bundleList)
                    }
                    bundle.clear()
                    bundleList?.remove(o.m3c)
                    for (i in  bundleList!!.indices) {
                        if (bundleList?.get(i)?.m3c!=null) {
                            m3c += bundleList?.get(i)?.m3c!!
                        }
                        if (bundleList?.get(i)?.quantityNos!=null) {
                            quantity += bundleList?.get(i)?.quantityNos!!
                        }
                        bundle.add(bundleList?.get(i)?.bundleNumber!!)
                    }
                    bundlescount = bundleList?.size!!
                    mView?.bundle?.text = bundlescount.toString()
                    val df = DecimalFormat("#.##")
                    val dx: String = df.format(m3c)

                    mView?.m3c?.text = dx.toString()
                    val pc = DecimalFormat("#.##")
                    val pcs: String = pc.format(quantity)

                    mView?.totalpcs?.text = pcs.toString()



                })
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }


}