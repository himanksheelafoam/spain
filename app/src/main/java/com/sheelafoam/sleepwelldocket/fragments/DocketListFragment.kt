package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.DocketListAdapter

import com.sheelafoam.sleepwelldocket.models.response.CustomerResponseModel
import com.sheelafoam.sleepwelldocket.models.response.DispatchListData
import com.sheelafoam.sleepwelldocket.models.response.DispatchListResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.PaginateListner
import com.sheelafoam.sleepwelldocket.utils.Utility

import kotlinx.android.synthetic.main.fragment_docketlist.view.*
import kotlinx.android.synthetic.main.fragment_docketlist.view.back
import kotlinx.android.synthetic.main.fragment_docketlist.view.customer_name
import kotlinx.android.synthetic.main.fragment_docketlist.view.recycler
import java.util.concurrent.TimeUnit

class DocketListFragment : BaseFragment(), RetrofitListener,CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var customerId: Int = 0
    var isFromPagination = false
    var currentPage: Int = 1
    var unitId:Int=0

    var docket: String = ""
    var from: String = ""
    var to: String = ""
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_docketlist, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!


        }
        controllerActivity.hideKeyboard()

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        RxView.clicks(mView?.filter).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                var bundle = Bundle()
                bundle.putInt(Constants.UNIT_ID, unitId)

                Utility.addFragment(controllerActivity, FilterFragment(), bundle)
            }
       // callapi()
        setPageListner()
        callListApi(currentPage!!)


        return mView.rootView
    }

    private fun callListApi(currentPage: Int) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(DispatchListResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.DISPATCHLIST)
        retrofit.setCall(retrofit.getApiInterface().getdispatchlist(customerId,docket,from,to,currentPage))

    }


    override fun onResume() {
        super.onResume()


    }

    private fun callapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(CustomerResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CUSTOMERS)
        retrofit.setCall(retrofit.getApiInterface().getcustomers(""))

    }
    private fun setPageListner() {


        mView?.recycler?.addOnScrollListener(object : PaginateListner() {
            override fun loadMoreItems() {
                currentPage++
                isFromPagination = true
                callListApi(currentPage!!)
            }

            override fun isLastPage(): Boolean {
                return false

            }

            override fun isLoading(): Boolean {
                return false
            }


        })
    }


    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.CUSTOMERS, ignoreCase = true)) {
            hideProgress()
            response as CustomerResponseModel
            if (response?.data != null) {
               /* mView?.customer_name.text = response?.data?.data?.get(0)?.displayName.toString()
                customerId = response?.data?.data?.get(0)?.id!!
              */


            }


        }else  if (requestType.equals(Constants.DISPATCHLIST, ignoreCase = true)) {
            hideProgress()
            response as DispatchListResponseModel
            mView?.count?.text=response.data?.count.toString()
            if (response.data!=null){
                if (response.data?.data!=null){
                    setAdapter(response.data?.data!!)
                }
            }

        }
    }

    private fun setAdapter(data: List<DispatchListData>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false
        )
        mView?.recycler?.adapter = DocketListAdapter(data!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }

    fun setdata(dockett: String, fromm: String, too: String, customer: Int, customername: String?) {
        controllerActivity.supportFragmentManager.popBackStack()
        docket=dockett
        from=fromm
        to=too
        customerId=customer
        mView?.customer_name.text=customername
        callListApi(currentPage)
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {

        if (o is DispatchListData){
            var bundle = Bundle()
            bundle.putSerializable(Constants.DISPATCHDETAIL, o)
            Utility.addFragment(controllerActivity,DispatchDetailFragment(),bundle)

        }
    }


}