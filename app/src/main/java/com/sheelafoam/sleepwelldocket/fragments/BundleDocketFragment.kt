package com.sheelafoam.sleepwelldocket.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.DocketProductAdapter
import com.sheelafoam.sleepwelldocket.models.request.ScanDetailsRequestModel
import com.sheelafoam.sleepwelldocket.models.response.BundleDetailResponseModel
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.SubmitResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_addproduct.view.*
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.*
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.back
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.customer_name
import kotlinx.android.synthetic.main.fragment_dispatchdocketproduct.view.unit
import java.text.DecimalFormat
import java.util.concurrent.TimeUnit

class BundleDocketFragment  : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String = ""
    var customername: String = ""
    var bundlescount: Int = 0
    var m3c: Double = 0.00
    var quantity: Double = 0.00
    lateinit var scanmodel: ScanDetailsRequestModel

    var bundle: ArrayList<String>?=null
    var bundlearraylist: ArrayList<String>?=null
    lateinit var bundleList: ArrayList<BundleListData>


    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_dispatchdocketproduct, container, false)

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        controllerActivity.hideKeyboard()

        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            customername = arguments?.getString(Constants.CUSTOMERS)!!
            mView?.unit?.text = unitname
            mView?.customer_name?.text = customername

            if ( arguments?.getSerializable(Constants.BUNDLE)!=null){
                bundle =
                    (arguments?.getSerializable(Constants.BUNDLE) as ArrayList<String>?)!!
                if (bundle != null) {

                    scanmodel = ScanDetailsRequestModel()
                    scanmodel.unit_id = unitId
                    scanmodel.customer_id = customerId
                    scanmodel.bundles = bundle
                    callBundleApi(scanmodel)
                    RxView.clicks( mView?.add_product).throttleFirst(2500, TimeUnit.MILLISECONDS)
                            .subscribe { empty: Void? ->
                                val bundles = Bundle()
                                bundles.putInt(Constants.UNIT_ID, unitId)
                                bundles.putString(Constants.UNITS, view?.unit!!.text.toString())
                                bundles.putInt(Constants.CUSTOMER_ID, customerId)
                                bundles.putString(Constants.CUSTOMERS, view?.customer_name!!.text.toString())
                                bundles.putSerializable(Constants.BUNDLE,bundle)

                                Utility.addFragment(controllerActivity, BatchScanFragment(), bundles)

                                // controllerActivity.onBackPressed()
                            }

                }
            }


           // mView?.add_product.visibility=View.GONE
            bundleList= ArrayList<BundleListData>()


        }
        bundlearraylist= ArrayList<String>()
        mView?.remove_product.visibility=View.GONE

        return mView.rootView
    }

    private fun callBundleApi(scanmodel: ScanDetailsRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(BundleDetailResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.SCANDETAILS)
        retrofit.setCall(retrofit.getApiInterface().bundledetails(scanmodel))

    }
    override fun onbackListen() {
        super.onbackListen()
        controllerActivity.clearAllFragments()
        Utility.replaceFragment(controllerActivity, DashBoardFragment(), null)
         }

    private fun callApi(scanmodel: ScanDetailsRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(SubmitResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.SUBMIT)
        retrofit.setCall(retrofit.getApiInterface().submit(scanmodel))

    }

    private fun setAdapter(bundleListData: ArrayList<BundleListData>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(
            getActivity(),
            LinearLayoutManager.VERTICAL,
            false
        )
        mView?.recycler?.adapter = DocketProductAdapter(bundleListData!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }


    override fun onResume() {
        super.onResume()
        controllerActivity.hideKeyboard()



    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.SUBMIT, ignoreCase = true)) {
            hideProgress()
            response as SubmitResponseModel
            var bundle = Bundle()
            bundle.putSerializable(Constants.SUBMIT, response.data?.data)

            Utility.addFragment(controllerActivity, SubmitDetailFragment(), bundle)

        }else  if (requestType.equals(Constants.SCANDETAILS, ignoreCase = true)) {
            hideProgress()
            response as BundleDetailResponseModel
            if (response.data != null) {
                if (response?.data?.data!=null)
                {

                        bundleList = (response?.data?.data as ArrayList<BundleListData>?)!!
                        if (bundleList != null && !bundleList.isEmpty()) {
                            setAdapter(bundleList)
                            mView?.bundle?.text = bundleList.size.toString()
                            for (i in bundleList.indices){
                                bundlearraylist?.add(bundleList.get(i).bundleNumber.toString())
                                if (bundleList?.get(i)?.m3c!=null) {
                                    m3c += bundleList?.get(i)?.m3c!!
                                }
                                if (bundleList?.get(i)?.quantityNos!=null) {
                                    quantity += bundleList?.get(i)?.quantityNos!!
                                }
                            }
                            val df = DecimalFormat("#.##")
                            val dx: String = df.format(m3c)

                            mView?.m3c?.text = dx.toString()
                            val pc = DecimalFormat("#.##")
                            val pcs: String = pc.format(quantity)

                            mView?.totalpcs?.text = pcs.toString()


                            RxView.clicks(mView?.submit).throttleFirst(2500, TimeUnit.MILLISECONDS)
                                    .subscribe { empty: Void? ->
                                        scanmodel = ScanDetailsRequestModel()
                                        scanmodel.unit_id = unitId
                                        scanmodel.customer_id = customerId
                                        scanmodel.bundles = bundlearraylist
                                        callApi(scanmodel)

                                    }
                        }
                }

            }
        }
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is BundleListData) {
            opendialog(o)

        }

    }
    private fun opendialog(o: BundleListData) {

        AlertDialog.Builder(controllerActivity)
            .setTitle(getString(R.string.remove_Product))
            .setMessage(getString(R.string.are_yousure)) // Specifying a listener allows you to take an action before dismissing the dialog.
            .setPositiveButton(android.R.string.yes,
                { dialog, which ->

                    m3c=0.00
                    quantity=0.00

                    bundleList?.remove(o)
                    if (bundleList!=null) {
                        setAdapter(bundleList)
                        mView?.bundle?.text = bundleList.size.toString()

                        bundlearraylist?.remove(o.bundleNumber)
                        for (i in bundleList.indices){
                            if (bundleList?.get(i)?.m3c!=null) {
                                m3c += bundleList?.get(i)?.m3c!!
                            }
                            if (bundleList?.get(i)?.quantityNos!=null) {
                                quantity += bundleList?.get(i)?.quantityNos!!
                            }
                        }
                        val df = DecimalFormat("#.##")
                        val dx: String = df.format(m3c)

                        mView?.m3c?.text = dx.toString()
                        val pc = DecimalFormat("#.##")
                        val pcs: String = pc.format(quantity)

                        mView?.totalpcs?.text = pcs.toString()



                    }

                })
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }



}