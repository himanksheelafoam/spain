package com.sheelafoam.sleepwelldocket.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.BundleAdapter
import com.sheelafoam.sleepwelldocket.models.request.ScanDetailsRequestModel
import com.sheelafoam.sleepwelldocket.models.response.ValidateListData
import com.sheelafoam.sleepwelldocket.models.response.ValidateResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_addproduct.view.*
import kotlinx.android.synthetic.main.fragment_aftervalidationdispatch.view.*
import kotlinx.android.synthetic.main.fragment_aftervalidationdispatch.view.back
import kotlinx.android.synthetic.main.fragment_aftervalidationdispatch.view.customer_name
import kotlinx.android.synthetic.main.fragment_aftervalidationdispatch.view.unit
import java.util.concurrent.TimeUnit

class AfterValidationDispatchFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    lateinit var scanmodel: ScanDetailsRequestModel
    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String = ""
    var customername: String = ""
    var invalidcount: Int = 0
    var validcount: Int = 0
    var bundles: ArrayList<String>?=null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_aftervalidationdispatch, container, false)
        controllerActivity.hideKeyboard()
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            customername = arguments?.getString(Constants.CUSTOMERS)!!
            bundles =
                (arguments?.getSerializable(Constants.BUNDLE) as ArrayList<String>?)!!
            mView?.unit?.text = unitname
            mView?.customer_name?.text = customername
            if (bundles != null && !bundles!!.isEmpty()) {
                scanmodel = ScanDetailsRequestModel()
                scanmodel.unit_id = unitId
                scanmodel.customer_id = customerId
                scanmodel.bundles = bundles
                callvalidateApi(scanmodel)
            }



        }


        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        RxView.clicks(mView?.add_product).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                if (!bundles!!.isEmpty()) {
                    var bundle = Bundle()
                    bundle.putInt(Constants.UNIT_ID, unitId)
                    bundle.putString(Constants.UNITS, view?.unit!!.text.toString())
                    bundle.putInt(Constants.CUSTOMER_ID, customerId)
                    bundle.putString(Constants.CUSTOMERS, view?.customer_name!!.text.toString())
                    bundle.putSerializable(Constants.BUNDLE, bundles)
                    Utility.addFragment(controllerActivity, BundleDocketFragment(), bundle)
                }
            }




        return mView.rootView
    }

    private fun callvalidateApi(scanmodel: ScanDetailsRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(ValidateResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.BUNDLE)
        retrofit.setCall(retrofit.getApiInterface().validatebundle(scanmodel))


    }


    override fun onResume() {
        super.onResume()
        controllerActivity.hideKeyboard()



    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.BUNDLE, ignoreCase = true)) {
            hideProgress()
            response as ValidateResponseModel
            if (response.data != null) {
                if (response.data?.data != null)
                    setAdapter(response.data?.data!!)


                for (i in response.data?.data?.indices!!) {
                    if (response.data?.data?.get(i)?.status == false) {
                        invalidcount = invalidcount + 1
                    } else {
                        validcount = validcount +1
                    }
                }
                mView?.invalid.text=invalidcount.toString()
                mView?.valid.text=validcount.toString()

            }

        }
    }

    private fun setAdapter(data: List<ValidateListData>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(
            getActivity(),
            LinearLayoutManager.VERTICAL,
            false
        )
        mView?.recycler?.adapter = BundleAdapter(data!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {

    }


}