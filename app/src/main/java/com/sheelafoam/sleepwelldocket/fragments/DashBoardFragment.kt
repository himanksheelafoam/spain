package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.models.response.UnitResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.fragment_dashboard.view.*

import java.util.concurrent.TimeUnit

class DashBoardFragment : BaseFragment(), RetrofitListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var unitId: Int = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_dashboard, container, false)
        callapi()
        RxView.clicks(mView.profile_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    Utility.addFragment(controllerActivity, ProfileFragment(), null)

                }
        val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.blob)
                .error(R.drawable.blob)
        if (AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.image != null) {
            Glide.with(controllerActivity.applicationContext).load(AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.profileImageUrl + "/" + AppPrefrences.Data.getInstance(controllerActivity).getUserModel(controllerActivity, "userData")?.user?.image!!).error(R.drawable.blob).apply(options).into(mView?.image)
        }

        mView.fullname.text = AppPrefrences.Data.getInstance(controllerActivity)
                .getUserModel(controllerActivity, "userData")?.user?.name


        mView.designation.text = AppPrefrences.Data.getInstance(controllerActivity)
                .getUserModel(controllerActivity, "userData")?.user?.designation

        RxView.clicks(mView.dispatch_docket_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    if (unitId != 0) {
                        val bundle = Bundle()
                        bundle.putInt(Constants.UNIT_ID, unitId)
                        bundle.putString(Constants.UNITS, mView.unit.text.toString())
                        Utility.addFragment(controllerActivity, DispatchDocketFragment(), bundle)
                    }
                }
        RxView.clicks(mView.dispatch_list_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    val bundle = Bundle()
                    bundle.putInt(Constants.UNIT_ID, unitId)
                    Utility.addFragment(controllerActivity, DocketListFragment(), bundle)
                }

        return mView.rootView
    }

    private fun callapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(UnitResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.UNITS)
        retrofit.setCall(retrofit.getApiInterface().getunits(""))

    }


    override fun onResume() {
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.UNITS, ignoreCase = true)) {
            hideProgress()
            response as UnitResponseModel
            if (response.data != null) {
                mView.unit?.text = response.data?.data?.get(0)?.name.toString()
                unitId = response?.data?.data?.get(0)?.id!!
                RxView.clicks(mView?.change_unit).throttleFirst(2500, TimeUnit.MILLISECONDS)
                        .subscribe { empty: Void? ->
                            var bundle = Bundle()
                            bundle.putSerializable(Constants.UNITS, response?.data)
                            Utility.addFragment(controllerActivity, SelectUnitFragment(), bundle)
                        }
            }


        }
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    fun setunit(o: UnitListData) {
        controllerActivity.supportFragmentManager.popBackStack()
        controllerActivity.runOnUiThread(Runnable {


            Thread.sleep(1000)


        })
        mView.unit.setText(o?.name!!)
        unitId = o?.id!!
    }



}


