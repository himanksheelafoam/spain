package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.DocketListAdapter
import com.sheelafoam.sleepwelldocket.models.request.CartDetailRequestModel
import com.sheelafoam.sleepwelldocket.models.response.BundleDetailResponseModel
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.CartListModel
import com.sheelafoam.sleepwelldocket.models.response.CartResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.bundle_cell.*
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.fragment_cart.view.back
import kotlinx.android.synthetic.main.fragment_dispatchdocket.view.*
import java.util.concurrent.TimeUnit

class CartFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    lateinit var model: CartDetailRequestModel
    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String? = null
    var type: String? = null
    var referencenumber=""
     var customername: String ?=null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_cart, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            customerId = arguments?.getInt(Constants.CUSTOMER_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            customername = arguments?.getString(Constants.CUSTOMERS)!!
        }
            controllerActivity.hideKeyboard()
        model = CartDetailRequestModel()

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    controllerActivity.onBackPressed()
                }


        callApi()


        return mView.rootView
    }

    private fun callApi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(CartResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CART)
        retrofit.setCall(retrofit.getApiInterface().cartlist())

    }

    private fun callDetailApi(model: CartDetailRequestModel) {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(BundleDetailResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CARTDETAIL)
        retrofit.setCall(retrofit.getApiInterface().cartdetails(model))

    }


    override fun onResume() {
        super.onResume()


    }


    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.CART, ignoreCase = true)) {
            hideProgress()
            response as CartResponseModel
            if (response?.data != null) {
                setAdapter(response?.data?.data)


            }


        } else if (requestType.equals(Constants.CARTDETAIL, ignoreCase = true)) {
            hideProgress()
            response as BundleDetailResponseModel
            if (response?.data != null) {
                var bundle = Bundle()
                bundle.putInt(Constants.UNIT_ID, unitId)
                bundle.putString(Constants.UNITS, unitname)
                bundle.putInt(Constants.CUSTOMER_ID, customerId)
                bundle.putString(
                        Constants.CUSTOMERS,
                        customername
                )

                bundle.putString(Constants.ACTIONTYPE,getString(R.string.add_product))
                bundle.putString(Constants.FROM,getString(R.string.cart))
                bundle.putString(Constants.REFERENCE,referencenumber)

                bundle.putSerializable(Constants.SCANDETAILS, response?.data?.data as ArrayList<BundleListData>)
                controllerActivity.hideKeyboard()
                Utility.addFragment(controllerActivity, AddProductFragment(), bundle)

            }


        }
    }

    private fun setAdapter(data: List<CartListModel>?) {
        mView?.recycler?.layoutManager = LinearLayoutManager(
                getActivity(),
                LinearLayoutManager.VERTICAL,
                false
        )
        mView?.recycler?.adapter = DocketListAdapter(data!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }


    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is CartListModel) {
            model.reference_number = o.referenceNumber.toString()
            referencenumber=o.referenceNumber.toString()
            callDetailApi(model)
        }

    }


}
