package com.sheelafoam.sleepwelldocket.fragments

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.UnitAdapter
import com.sheelafoam.sleepwelldocket.models.response.CustomerListData
import com.sheelafoam.sleepwelldocket.models.response.CustomerResponseModel
import com.sheelafoam.sleepwelldocket.models.response.DocketListData
import com.sheelafoam.sleepwelldocket.models.response.DocketResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.functions.Predicate
import kotlinx.android.synthetic.main.fragment_filter.view.*
import kotlinx.android.synthetic.main.fragment_select_unit.view.*
import kotlinx.android.synthetic.main.open_bottom_sheet.view.*
import kotlinx.android.synthetic.main.open_bottom_sheet.view.search_view
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class FilterFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var datePickerDialog: DatePickerDialog? = null
    val myCalendar = Calendar.getInstance()
    var chosenDate: Date? = null
    var customerId: Int = 0
    var docketId: String = ""
    var dialog: BottomSheetDialog? = null
    var unitId: Int = 0
    private var searchRequest: SearchRequest? = null
    var customerlist: ArrayList<CustomerListData>? = null
    lateinit var  btnsheet:View

    var docketListFragment: DocketListFragment? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!


        }
        controllerActivity.hideKeyboard()


        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    controllerActivity.onBackPressed()
                }
        RxView.clicks(mView?.from_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    openCalender(true)
                }
        RxView.clicks(mView?.to_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    openCalender(false)
                }

        RxView.clicks(mView?.clear).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    mView?.from.setText("")
                    mView?.to.setText("")
                    mView?.docket_number.setText("")
                    docketId = ""

                }
        RxView.clicks(mView?.apply).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    docketListFragment =
                            controllerActivity!!.supportFragmentManager.findFragmentByTag(DocketListFragment::class.java.getName()) as DocketListFragment?
                    docketListFragment?.setdata(
                            docketId,
                            mView.from.text.toString(),
                            mView.to.text.toString(),
                            customerId,
                            mView?.customers?.text?.toString()
                    )
                }
        callapi()


        return mView.rootView
    }

    private fun callapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(CustomerResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CUSTOMERS)
        retrofit.setCall(retrofit.getApiInterface().getcustomers(searchRequest?.search))

    }

    private fun calldocketapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(DocketResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.DOCKET)
        retrofit.setCall(retrofit.getApiInterface().getdocketlist(unitId, customerId))

    }

    private fun openCalender(isFromDate: Boolean) {
        val date =
                OnDateSetListener { view, year, monthOfYear, dayOfMonth -> // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year)
                    myCalendar.set(Calendar.MONTH, monthOfYear)
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    if (isFromDate) {
                        chosenDate = myCalendar.getTime()
                    }
                    setdate(isFromDate)
                }
        datePickerDialog = DatePickerDialog(
                controllerActivity, date, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        )
        if (isFromDate) {
            datePickerDialog!!.getDatePicker().setMaxDate(Date().time)
        } else {
            datePickerDialog!!.getDatePicker().setMaxDate(Date().time)
            if (chosenDate != null) {
                datePickerDialog!!.getDatePicker().setMinDate(chosenDate?.getTime()!!)
                datePickerDialog!!.getDatePicker().setMaxDate(Date().time)
            }
        }
        datePickerDialog!!.show()
    }

    private fun setdate(isFromDate: Boolean) {
        val myFormat = "dd/MM/yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        if (isFromDate) {
            mView?.from.setText(sdf.format(myCalendar.getTime()))
        } else {
            mView?.to.setText(sdf.format(myCalendar.getTime()))

        }
    }


    override fun onResume() {
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.CUSTOMERS, ignoreCase = true)) {
            hideProgress()
            response as CustomerResponseModel
            if (response?.data != null) {
                mView?.customers.setText(response?.data?.data?.get(0)?.displayName.toString()!!)
                customerId = response?.data?.data?.get(0)?.id!!
                customerlist = response?.data?.data as ArrayList<CustomerListData>?
               // customerlist?.let { setAdapter(it) }
                RxView.clicks(mView?.customer_layout).throttleFirst(2500, TimeUnit.MILLISECONDS)
                        .subscribe { empty: Void? ->
                            openbottomsheet(response?.data?.data as ArrayList<CustomerListData>)
                        }
                RxView.clicks(mView?.docket_down).throttleFirst(2500, TimeUnit.MILLISECONDS)
                        .subscribe { empty: Void? ->
                            calldocketapi()
                        }
            }


        } else if (requestType.equals(Constants.DOCKET, ignoreCase = true)) {
            hideProgress()
            response as DocketResponseModel
            if (response?.data != null) {
                //   mView?.docket_number.setText(response?.data?.data?.get(0)?.displayName.toString()!!)
                openbottomsheet(response?.data?.data!!)

            }


        }
    }

   fun filter(text: String) {
        val invitedPeople= ArrayList<CustomerListData>()

         for (e in customerlist?.indices!!) {
            if (customerlist?.get(e)?.displayName?.toLowerCase()?.contains(text.toLowerCase())!!) {
                invitedPeople.add(customerlist?.get(e)!!)
            }
        }

      setAdapter(invitedPeople)



   }

    private fun openbottomsheet(data: List<*>) {
        btnsheet = controllerActivity.layoutInflater.inflate(R.layout.open_bottom_sheet, null)
        dialog = BottomSheetDialog(controllerActivity)
        dialog!!.setContentView(btnsheet)

        btnsheet.tv_close.setOnClickListener {
            dialog!!.dismiss()
        }
        val mBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(btnsheet.parent as View)
        val metrics = controllerActivity.resources.displayMetrics
        mBehavior.peekHeight = metrics.heightPixels
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
         btnsheet.search_view.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                filter(s.toString())

            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        searchRequest = SearchRequest()
        btnsheet?.search_view?.setHint("Search customers")

        setAdapter(data)
        dialog!!.show()

    }

    private fun setAdapter(data: List<*>) {
        btnsheet.bottom_recycler.layoutManager =
                LinearLayoutManager(controllerActivity, LinearLayoutManager.VERTICAL, false)
        btnsheet.bottom_recycler.adapter = UnitAdapter(data, this)

    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is CustomerListData) {
            mView?.customers.setText(o?.displayName!!)
            customerId = o?.id!!
            dialog?.dismiss()
        } else if (o is DocketListData) {
            mView?.docket_number.setText(o?.app_pl_number!!)
            docketId = o?.app_pl_number!!
            dialog?.dismiss()
        }
    }


}