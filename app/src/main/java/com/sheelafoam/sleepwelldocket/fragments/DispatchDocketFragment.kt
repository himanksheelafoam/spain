package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.models.response.CustomerListData
import com.sheelafoam.sleepwelldocket.models.response.CustomerResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility

import kotlinx.android.synthetic.main.fragment_dispatchdocket.view.*
import kotlinx.android.synthetic.main.fragment_dispatchdocket.view.unit
import java.util.concurrent.TimeUnit

class DispatchDocketFragment : BaseFragment(), RetrofitListener {
    private var activity: Context? = null
    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    var unitId: Int = 0
    var customerId: Int = 0
    var unitname: String = ""
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_dispatchdocket, container, false)
        if (arguments != null) {
            unitId = arguments?.getInt(Constants.UNIT_ID)!!
            unitname = arguments?.getString(Constants.UNITS)!!
            mView?.unit?.text = unitname.toString()


        }
        controllerActivity.hideKeyboard()

        RxView.clicks(mView?.back).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                controllerActivity.onBackPressed()
            }
        RxView.clicks(mView?.cart).throttleFirst(2500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    if (isValidate()) {
                        var bundle = Bundle()
                        bundle.putInt(Constants.UNIT_ID, unitId)
                        bundle.putString(Constants.UNITS, mView?.unit.text.toString())
                        bundle.putInt(Constants.CUSTOMER_ID, customerId)
                        bundle.putString(
                                Constants.CUSTOMERS,
                                mView?.customer_name.text.toString()
                        )
                        bundle.putString(Constants.ACTIONTYPE,getString(R.string.add_product))

                        Utility.addFragment(controllerActivity,CartFragment(),bundle)

                             }
                       }
        RxView.clicks(mView?.add_product).throttleFirst(2500, TimeUnit.MILLISECONDS)
            .subscribe { empty: Void? ->
                if (Utility.checkAndRequestGalleryPermissions(controllerActivity) && Utility.checkAndRequestCameraPermissions(
                        controllerActivity
                    )
                ) {
                    if (unitId != 0) {
                        if (isValidate()) {
                            var bundle = Bundle()
                            bundle.putInt(Constants.UNIT_ID, unitId)
                            bundle.putString(Constants.UNITS, mView?.unit.text.toString())
                            bundle.putInt(Constants.CUSTOMER_ID, customerId)
                            bundle.putString(
                                Constants.CUSTOMERS,
                                mView?.customer_name.text.toString()
                            )
                            bundle.putString(Constants.ACTIONTYPE,getString(R.string.add_product))
                            bundle.putString(Constants.FROM,getString(R.string.dispatch_docket))
                            bundle.putString(Constants.REFERENCE,"")




                            Utility.addFragment(controllerActivity, AddProductFragment(), bundle)
                        }
                    }
                }
            }
        callapi()

        return mView.rootView
    }

    private fun isValidate(): Boolean {
        if (customerId==0){
            Utility.errorToast(controllerActivity,getString(R.string.please_select_customer))
            return false
        }
        return true

    }


    override fun onResume() {
        super.onResume()


    }

    private fun callapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(CustomerResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CUSTOMERS)
        retrofit.setCall(retrofit.getApiInterface().getcustomers(""))

    }


    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.CUSTOMERS, ignoreCase = true)) {
            hideProgress()
            response as CustomerResponseModel
            if (response?.data != null) {
                  RxView.clicks(mView?.btn_select).throttleFirst(2500, TimeUnit.MILLISECONDS)
                    .subscribe { empty: Void? ->
                        var bundle = Bundle()
                        bundle.putSerializable(Constants.CUSTOMERS, response?.data)
                        Utility.addFragment(controllerActivity, SelectCustomerFragment(), bundle)
                    }
            }


        }
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    fun setcustomer(o: CustomerListData) {
        controllerActivity.supportFragmentManager.popBackStack()
        controllerActivity.runOnUiThread(Runnable {


            Thread.sleep(1000)


        })
        controllerActivity.hideKeyboard()
        mView?.customer_name.setText(o?.displayName!!)
        customerId = o?.id!!
    }
    fun setback() {
        controllerActivity.supportFragmentManager.popBackStack()

    }


}