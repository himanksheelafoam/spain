package com.sheelafoam.sleepwelldocket.fragments

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.activities.ControllerActivity
import com.sheelafoam.sleepwelldocket.adapter.UnitAdapter
import com.sheelafoam.sleepwelldocket.models.response.CustomerListData
import com.sheelafoam.sleepwelldocket.models.response.CustomerResponseModel
import com.sheelafoam.sleepwelldocket.models.response.Customerdata
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.*
import kotlinx.android.synthetic.main.fragment_select_unit.view.*


class SelectCustomerFragment : BaseFragment(), RetrofitListener, CommonItemClickListener {
    private var activity: Context? = null
    private var searchRequest: SearchRequest? = null
    var dispatchDocketFragment: DispatchDocketFragment? = null
    var customerdata: Customerdata? = null
    var customerListData: ArrayList<CustomerListData>? = null


    private var controllerActivity: ControllerActivity = ControllerActivity()
    lateinit var mView: View
    override fun onAttach(context: Context) {
        super.onAttach(context)
        controllerActivity = context as ControllerActivity
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_unit, container, false)
        searchRequest = SearchRequest()
        mView?.search_view.setHint("Search customers")
        mView?.tap_title?.text = "Tap to select the customer"
        mView?.search_view.requestFocus()
        controllerActivity.showKeyboard()
         if (arguments != null) {
            customerdata =
                    arguments?.getSerializable(Constants.CUSTOMERS) as Customerdata?
            if (customerdata != null) {
                customerListData = customerdata?.data as ArrayList<CustomerListData>?
                setAdapter(customerListData!!)
            }
        }
        mView.search_view.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                filter(s.toString())

            }

            override fun afterTextChanged(s: Editable) {

            }
        })





        return mView.rootView
    }

    private fun callapi() {
        showProgress(controllerActivity)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(controllerActivity)
        retrofit.setResponseType(CustomerResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.CUSTOMERS)
        retrofit.setCall(retrofit.getApiInterface().getcustomers(searchRequest?.search))

    }



    fun filter(text: String) {
        val invitedPeople = ArrayList<CustomerListData>()

        for (e in customerListData?.indices!!) {
            if (customerListData?.get(e)?.displayName?.toLowerCase()?.contains(text.toLowerCase())!!) {
                invitedPeople.add(customerListData?.get(e)!!)
            }
        }

        setAdapter(invitedPeople)


    }


    override fun onResume() {
        super.onResume()


    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.CUSTOMERS, ignoreCase = true)) {
            hideProgress()
            response as CustomerResponseModel
            if (response?.data != null) {
                setAdapter(response?.data?.data!! as ArrayList<CustomerListData>)
            }


        }
    }

    private fun setAdapter(data: ArrayList<CustomerListData>) {
        mView?.recycler?.layoutManager = LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false)
        mView?.recycler?.adapter = UnitAdapter(data!!, this)
        mView?.recycler.setNestedScrollingEnabled(false)

    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(controllerActivity, responseMessage)

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is CustomerListData) {
            dispatchDocketFragment = controllerActivity!!.supportFragmentManager.findFragmentByTag(DispatchDocketFragment::class.java.getName()) as DispatchDocketFragment?
            dispatchDocketFragment?.setcustomer(o!!)
        }
    }


}