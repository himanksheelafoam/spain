package com.sheelafoam.sleepwelldocket.activities

import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.jakewharton.rxbinding.view.RxView
import com.sheelafoam.sleepwelldocket.BuildConfig
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.adapter.BottomAdapter
import com.sheelafoam.sleepwelldocket.adapter.UnitAdapter
import com.sheelafoam.sleepwelldocket.models.request.LoginRequestModel
import com.sheelafoam.sleepwelldocket.models.response.LoginResponseModel
import com.sheelafoam.sleepwelldocket.retrofit.MyRetrofit
import com.sheelafoam.sleepwelldocket.retrofit.RetrofitListener
import com.sheelafoam.sleepwelldocket.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.open_bottom_sheet.view.*
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


class LoginActivity : BaseActivity(), CommonItemClickListener, RetrofitListener {
    var versionCode: Int? = 0
    var firebase_shared: SharedPreferences? = null
    var latitude = ""
    var longitute = ""
    lateinit var model: LoginRequestModel
    var firebase_reg_id = ""
    var dialog: BottomSheetDialog? = null

    var first_trigger = false
    lateinit var language: ArrayList<String>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        try {
            val pInfo: PackageInfo = this.packageManager.getPackageInfo(this.getPackageName(), 0)
            versionCode = pInfo.versionCode
            firebase_shared = getSharedPreferences("ah_firebase", 0)
            firebase_reg_id = firebase_shared!!.getString("regId", "")!!

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        language= ArrayList<String>()
        language.add(getString(R.string.english))
        language.add(getString(R.string.spanish))
        RxView.clicks(dropdown).throttleFirst(1500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    if (language!=null){
                    openbottomsheet(language)
                }
                }

                setApplicationLanguage()

        if (Utility.checkLocationPermission(this)) {

            LocationUpdateManager(
                    this,
                    this,
                    CommonItemClickListener { o: Any?, isFrom: String?, position: String? ->
                        this.onItemClickListener(
                                o,
                                isFrom,
                                position
                        )
                    })
        }

        RxView.clicks(next).throttleFirst(1500, TimeUnit.MILLISECONDS)
                .subscribe { empty: Void? ->
                    if (isValidate()) {
                        val androidId =
                                Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

                        model = LoginRequestModel()
                        model.username = etemail.text.toString()
                        model.password = etpassword.text.toString()
                        model.grant_type = "password"
                        model.latitude = latitude
                        model.longitude = longitute
                        model.client_id = BuildConfig.CLIENT_ID
                        model.client_secret = BuildConfig.CLIENT_SECRET
                        model.app_version = versionCode.toString()
                        model.device_name = Utility.getDeviceName()
                        model.device_address = androidId
                        model.os_version = Utility.getDeviceOs()
                        if (firebase_reg_id == null) {
                        } else {
                            model.firebase_id = firebase_reg_id
                            Log.i("firebase_id", firebase_reg_id)
                        }

                        login(model)
                    }
                }
        RxView.clicks(eye_show).throttleFirst(1500, TimeUnit.MILLISECONDS).subscribe { empty: Void? ->
            if (!etpassword.text.equals("")) {
                etpassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                hide_eye.visibility = View.VISIBLE
                eye_show.visibility = View.GONE
            }

        }
        RxView.clicks(hide_eye).throttleFirst(1500, TimeUnit.MILLISECONDS).subscribe { empty: Void? ->
            if (!etpassword.text.equals("")) {
                etpassword.setTransformationMethod(PasswordTransformationMethod.getInstance())
                eye_show.visibility = View.VISIBLE
                hide_eye.visibility = View.GONE
            }

        }

    }



    private fun login(model: LoginRequestModel) {
        showProgress(this)
        val retrofit: MyRetrofit = MyRetrofit.getInstance(this)
        retrofit.setResponseType(LoginResponseModel::class.java)
        retrofit.setListener(this)
        retrofit.setRequestTag(Constants.LOGIN)
        retrofit.setCall(retrofit.getApiInterface().login(model))

    }

    private fun isValidate(): Boolean {
        if (etemail.getText().toString().equals("")) {
            Utility.errorToast(this, getString(R.string.please_enter_email_phone))
            return false
        } else if (etpassword.getText().toString().equals("")) {
            Utility.errorToast(this, getString(R.string.please_enter_password))
            return false;
        }

        return true

    }

    override fun onItemClickListener(o: Any?, isFrom: String?, position: String?) {
        if (o is Location) {
            val location = o as Location
            val addresses: List<Address>?
            val geoCoder = Geocoder(applicationContext, Locale.getDefault())
            addresses = geoCoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    1
            )
            latitude = location?.latitude.toString()!!
            longitute = location?.longitude.toString()!!

        }else if (o is String){
            tvlanguage.setText(o)
            if (o.equals(getString(R.string.english))) {
                AppPrefrences.Data.getInstance(this@LoginActivity).setLanguage("en")
                setApplicationLocale()
                     val i = Intent(this@LoginActivity, LoginActivity::class.java)
                startActivity(i)
                finish()


            } else if (o.equals(getString(R.string.spanish))) {
                AppPrefrences.Data.getInstance(this@LoginActivity).setLanguage("es")
                 setApplicationLocale()
                val i = Intent(this@LoginActivity, LoginActivity::class.java)
                startActivity(i)
                finish()

            }

        }
    }

    override fun onSuccess(requestType: String?, response: Any?, responseMessage: String?) {
        if (requestType.equals(Constants.LOGIN, ignoreCase = true)) {
            hideProgress()
            response as LoginResponseModel
            startActivity(Intent(this, ControllerActivity::class.java))
            AppPrefrences.Data.getInstance(this).saveUserModel(response.data, "userData", this)
            AppPrefrences.Data.getInstance(this).setAccessToken(response.data?.access_token)

            //   openbottomsheet(response.)
        }
    }

    override fun onFailure(requestType: String?, responseMessage: String?, responseCode: Int) {
        hideProgress()
        Utility.errorToast(this, responseMessage)

    }

    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
    }
    fun setApplicationLanguage() {
        val lang: String = AppPrefrences.Data.getInstance(this).getLanguage()!!
        if (lang.equals("en", ignoreCase = true)) {
            LocaleHelper.setLocale(this, "en")
            tvlanguage.setText(getString(R.string.english))
        } else if (lang.equals("es", ignoreCase = true)) {
            LocaleHelper.setLocale(this, "es")
            tvlanguage.setText(getString(R.string.spanish))
        }
    }
    fun setApplicationLocale() {
        val lang: String = AppPrefrences.Data.getInstance(this).getLanguage()!!
        if (lang.equals("en", ignoreCase = true)) {
            LocaleHelper.setLocale(this, "en")
           // AppPrefrences.Data.getInstance(this@LoginActivity).setLanguage("en")
        } else {
            LocaleHelper.setLocale(this, "es")
           // AppPrefrences.Data.getInstance(this@LoginActivity).setLanguage("es")
        }
    }
    private fun openbottomsheet(data: List<*>) {
        val btnsheet = layoutInflater.inflate(R.layout.open_bottom_sheet, null)
        dialog = BottomSheetDialog(this)
        btnsheet.tv_close.setOnClickListener {
            dialog!!.dismiss()
        }

        btnsheet.bottom_recycler.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        btnsheet.bottom_recycler.adapter = BottomAdapter(data, this)
        dialog!!.setContentView(btnsheet)
        dialog!!.show()

    }
}