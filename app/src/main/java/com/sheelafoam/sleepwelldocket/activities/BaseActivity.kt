package com.sheelafoam.sleepwelldocket.activities

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.kaopiz.kprogresshud.KProgressHUD

open class BaseActivity : AppCompatActivity() {
    var hud: KProgressHUD? = null


    public override fun onResume() {
        super.onResume()
    }

    /**
     * This function is call when we need show progress bar
     * @param context
     */
    protected open fun showProgress(context: Context?): Unit {
        Log.d("Progress", "baseActiviry")
        if (context != null) {
            hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setAnimationSpeed(2)
            hud?.show()

        }
    }

    protected open fun hideProgress() {
        Log.d("Progress", "baseActiviry")
        hud?.dismiss()
    }

}
