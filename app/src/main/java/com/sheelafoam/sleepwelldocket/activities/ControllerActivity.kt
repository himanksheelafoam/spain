package com.sheelafoam.sleepwelldocket.activities

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.fragments.BundleDocketFragment
import com.sheelafoam.sleepwelldocket.fragments.DashBoardFragment
import com.sheelafoam.sleepwelldocket.fragments.DispatchDocketProductFragment
import com.sheelafoam.sleepwelldocket.fragments.SubmitDetailFragment
import com.sheelafoam.sleepwelldocket.utils.Utility

class ControllerActivity : BaseActivity() {
    private var doubleBackToExit = false
    private var fmanager: FragmentManager? = null
    private var curFragment: Fragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_controller)
        Utility.addFragment(this, DashBoardFragment(), null)
    }
    override fun onAttachFragment(fragment: Fragment) {
        this.curFragment = fragment
    }

    override fun onBackPressed() {

            handleBackStack()

        Handler().postDelayed({ doubleBackToExit = false }, 2000)
    }


    private fun handleBackStack() {
        fmanager = supportFragmentManager
        val count: Int = supportFragmentManager.fragments.size
        if (curFragment is BundleDocketFragment) {
            (curFragment as BundleDocketFragment).onbackListen()
        }else  if (curFragment is DispatchDocketProductFragment) {
            (curFragment as DispatchDocketProductFragment).onbackListen()
        }else  if (curFragment is SubmitDetailFragment) {
            (curFragment as SubmitDetailFragment).onbackListen()
        }
        else if (count > 1) {
            val fragment =
                supportFragmentManager.fragments[count - 2]
            supportFragmentManager.popBackStack()
            fragment.onResume()
        } else {
            if (doubleBackToExit) {
                finishAffinity()
            } else {
                Toast.makeText(this, getString(R.string.press_again_to_exit), Toast.LENGTH_SHORT).show()
                doubleBackToExit = true
            }
        }
    }


    fun clearAllFragments() {
        val fm = supportFragmentManager
        for (i in 0..fm.backStackEntryCount) {
            fm.popBackStack()
        }
    }


    fun hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }
    fun showKeyboard() {
        showkeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
    fun Context.showkeyboard(view: View) {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)   }

}