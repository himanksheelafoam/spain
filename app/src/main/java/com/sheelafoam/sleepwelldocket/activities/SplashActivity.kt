package com.sheelafoam.sleepwelldocket.activities

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*

class SplashActivity : AppCompatActivity() {
    var versionCode: Int? = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        var mContext: Context? = null
        var logIn_session = false
        var access_token: String? = null
        var ismobileverify:String=""
        Handler().postDelayed({
            mContext = applicationContext
            access_token = AppPrefrences.Data.getInstance(this).getAccessToken()
           if (AppPrefrences.Data.getInstance(this).getLanguage().equals("es")) {
               setLocale("es")
           }else{
               setLocale("en")
           }
            try {
                val pInfo: PackageInfo = this.packageManager.getPackageInfo(this.getPackageName(), 0)
                versionCode = pInfo.versionCode
                version.setText(getString(R.string.version)+ " "+versionCode.toString())

            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            if (access_token.equals("", ignoreCase = true)) {
                val i = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(i)
                finish()

            } else {

                val i = Intent(this@SplashActivity, ControllerActivity::class.java)
                startActivity(i)
                finish()


            }
        }, SPLASH_TIME_OUT.toLong())


    }
    companion object {
        internal const val SPLASH_TIME_OUT = 5000
    }

    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res = resources
        val dm = res.displayMetrics
        val conf = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
   }

}