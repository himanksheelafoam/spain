package com.sheelafoam.sleepwelldocket.retrofit;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences;

import org.json.JSONObject;

import java.lang.reflect.Type;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MyRetrofit implements Callback<ResponseBody> {
    private static String token = "";
    private String requestTag;
    private Type responseType;
    private RetrofitListener listener;
    Call<ResponseBody> userCall;
    static MyRetrofit instance;
    static Context context;

    public static MyRetrofit getInstance(Context apiContext) {

        context = apiContext;
        token = AppPrefrences.Data.getInstance(apiContext).getAccessToken();
        return instance = new MyRetrofit();
    }

    public MyRetrofit() {

    }

    public void setCall(Call<ResponseBody> userCall) {
        this.userCall = userCall;
        this.userCall.enqueue(this);
    }

    public ApiInterface getApiInterface() {
        Retrofit builder;
        builder = ApiClient.getClient(token, context);
        Log.d("token", "" + token);
        ApiInterface apiInterface;
        apiInterface = builder.create(ApiInterface.class);
        return apiInterface;
    }

    public ApiInterface getAuthApiInterface() {
        Retrofit builder;
        builder = ApiClient.getClient(token, context);
        Log.d("token", "" + token);
        ApiInterface apiInterface;
        apiInterface = builder.create(ApiInterface.class);
        return apiInterface;
    }

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        Object resultObject = null;
        try {
            if (response != null && response.isSuccessful()) {
                String responseBody = response.body().string();
                Log.e("Response", "=" + responseBody);
                Gson gson = new Gson();

                resultObject = gson.fromJson(responseBody, responseType);
                listener.onSuccess(requestTag, resultObject, response.message());
            } else {
                Log.e("Response", "=" + response.body().string());
                listener.onFailure(requestTag, "Network Error", response.code());
            }
        } catch (Exception e) {
            Log.e("Exception in parsing", "=" + e.getLocalizedMessage());
//            listener.onFailure(requestTag, "Data Not Found!");
            try {
                String errorResponse = response.errorBody().string();
                JSONObject object = new JSONObject(errorResponse);
                JSONObject jsonObject = object.getJSONObject("errors");
                String message = "";

                if (jsonObject.has("message"))
                    message = String.valueOf(jsonObject.getString("message"));
                listener.onFailure(requestTag, "" + message, response.code());
            } catch (Exception e1) {
                listener.onFailure(requestTag, "", response.code());
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
        Log.e("Response", "=" + throwable.getMessage());
        listener.onFailure(requestTag, "" + throwable.getLocalizedMessage(), 0);
    }


    public void setRequestTag(String requestTag) {
        this.requestTag = requestTag;
    }


    public void setResponseType(Type responseType) {
        this.responseType = responseType;
    }

    public void setListener(RetrofitListener listener) {
        this.listener = listener;
    }

}