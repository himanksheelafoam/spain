package com.sheelafoam.sleepwelldocket.retrofit;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import com.sheelafoam.sleepwelldocket.BuildConfig;
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences;

public class ApiClient {
    public static OkHttpClient.Builder httpClient;
    private static Retrofit.Builder builder;




    public static Retrofit getClient(final String token, Context context) {
        httpClient = new OkHttpClient.Builder();
        Retrofit retrofit = null;
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        builder = new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create());

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request newRequest = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", "Bearer "+ AppPrefrences.Data.getInstance(context).getAccessToken())
//                        .addHeader("fcmtoken", App_prefrences.getFcmToken(context))

                        .build();
                Log.d("token", "" + token);
                return chain.proceed(newRequest);
            }
        }).addInterceptor(logging).readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();



//
//

        OkHttpClient client = httpClient.build();
        retrofit = builder.client(client).build();
        return retrofit;


    }


    // We need a new client, since we don't want to make another call using our client with access token
//                Call<AccessToken> call = tokenClient.getRefreshAccessToken(mToken.getRefreshToken(),
//                        mToken.getClientID(), mToken.getClientSecret(), API_OAUTH_REDIRECT,
//                        "refresh_token");

}