package com.sheelafoam.sleepwelldocket.retrofit;

import com.sheelafoam.sleepwelldocket.models.request.CartDetailRequestModel;
import com.sheelafoam.sleepwelldocket.models.request.LoginRequestModel;
import com.sheelafoam.sleepwelldocket.models.request.SaveDraftRequestModel;
import com.sheelafoam.sleepwelldocket.models.request.ScanDetailsRequestModel;
import com.sheelafoam.sleepwelldocket.models.request.ScanRequestModel;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {
    @POST("/erp/login")
    Call<ResponseBody> login(@Body LoginRequestModel model);
    @GET("/dispatch_docket/units")
    Call<ResponseBody> getunits(@Query(value = "keyword", encoded = true) String keyword);
    @GET("/dispatch_docket/customers")
    Call<ResponseBody> getcustomers(@Query(value = "keyword", encoded = true) String keyword);
    @POST("/dispatch_docket/scan")
    Call<ResponseBody> scandetails(@Body ScanRequestModel model);
    @POST("/dispatch_docket/bundle-details")
    Call<ResponseBody> bundledetails(@Body ScanDetailsRequestModel model);
    @POST("/dispatch_docket/validate")
    Call<ResponseBody> validatebundle(@Body ScanDetailsRequestModel model);
    @POST("/dispatch_docket/add-cart")
    Call<ResponseBody> saveasdraft(@Body SaveDraftRequestModel model);

    @POST("dispatch_docket/submit")
    Call<ResponseBody> submit(@Body ScanDetailsRequestModel model);
    @GET("dispatch_docket/dispatch-listing")
    Call<ResponseBody> getdispatchlist(@Query(value = "customer_id", encoded = true) int customer,
                                        @Query(value = "docket", encoded = true) String docket,
                                        @Query(value = "from", encoded = true) String from,
                                        @Query(value = "to", encoded = true) String to,
                                        @Query(value = "page", encoded = true) int pageIndex);
    @GET("dispatch_docket/dispatch-bundle")
    Call<ResponseBody> getdispatchlistdetail(@Query(value = "docket", encoded = true) String docket);

    @GET("dispatch_docket/docket-list")
    Call<ResponseBody> getdocketlist(@Query(value = "unit_id", encoded = true) int unitid,
                                     @Query(value = "customer_id", encoded = true) int customerid );

    @GET("dispatch_docket/release?")
    Call<ResponseBody> deletedocket(@Query(value = "id", encoded = true) int id);
    @GET("dispatch_docket/cart-list")
    Call<ResponseBody> cartlist();
    @POST("dispatch_docket/cart-details")
    Call<ResponseBody> cartdetails(@Body CartDetailRequestModel model);


}
