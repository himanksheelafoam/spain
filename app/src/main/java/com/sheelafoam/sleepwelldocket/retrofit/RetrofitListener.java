package com.sheelafoam.sleepwelldocket.retrofit;

public interface RetrofitListener {
    void onSuccess(String requestType, Object response, String responseMessage) ;

    void onFailure(String requestType, String responseMessage, int responseCode);

}
