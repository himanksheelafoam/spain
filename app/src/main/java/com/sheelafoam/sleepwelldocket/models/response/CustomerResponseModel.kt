package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CustomerResponseModel  : Serializable {
    @SerializedName("status")
    val status: Int? =0

    @SerializedName("data")
    val data: Customerdata? = null


}

class Customerdata :Serializable{
    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
     var data: List<CustomerListData>? = null
}

class CustomerListData :Serializable{
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("display_name")
    var displayName: String? = null
}
