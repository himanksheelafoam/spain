package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class UnitResponseModel :Serializable {
    @SerializedName("status")
    val status: Int? =0

    @SerializedName("data")
    val data: Unitdata? = null


}

class Unitdata:Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
     var data: List<UnitListData>? = null
}

class UnitListData:Serializable {

    @SerializedName("id")
     var id: Int? = null

    @SerializedName("code")
    var code: String? = null

    @SerializedName("name")
    var name: String? = null
}
