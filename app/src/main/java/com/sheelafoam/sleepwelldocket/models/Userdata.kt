package com.sheelafoam.sleepwelldocket.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class Userdata(
    @SerializedName("token_type")
    var tokenType: String="",

    @SerializedName("expires_in")
    var expiresIn: Int=0,

    @SerializedName("access_token")
    var access_token: String="",

    @SerializedName("refresh_token")
    var refreshToken: String="",

    @SerializedName("user")
    var user: UserInnerdata,

    @SerializedName("currency")
    var currency: String="",

    @SerializedName("customer_offer")
    var customerOffer: String="",

    @SerializedName("menu_icon_url")
    var menuIconUrl: String="",

    @SerializedName("profile_image_url")
    var profileImageUrl: String="",

    @SerializedName("force_updation")
    var forceUpdation: Boolean,

    @SerializedName("roles")
    var roles: List<String>,




)

