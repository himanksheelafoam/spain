package com.sheelafoam.sleepwelldocket.models.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CartDetailRequestModel :Serializable{
    @SerializedName("reference_number")
    var reference_number: String? = null

}