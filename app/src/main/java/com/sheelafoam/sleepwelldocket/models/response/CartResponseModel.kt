package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CartResponseModel :Serializable{
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: CartDataModel? = null
}

class CartDataModel :Serializable{

    @SerializedName("message")
    var message: String? = null

    @SerializedName("count")
     var count: Int? = null

    @SerializedName("data")
     var data: List<CartListModel>? = null
}

class CartListModel:Serializable {
    @SerializedName("createdby")
    var createdby: String? = null

    @SerializedName("reference_number")
    var referenceNumber: String? = null

    @SerializedName("date")
    var date: String? = null

    @SerializedName("customer")
    var customer: String? = null
}
