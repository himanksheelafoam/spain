package com.sheelafoam.sleepwelldocket.models.request

import com.google.gson.annotations.SerializedName

class ScanRequestModel {
    @SerializedName("unit_id")
    var unit_id: Int? = 0

    @SerializedName("customer_id")
    var customer_id: Int? = 0

    @SerializedName("bundle")
    var bundles: String? = null
}
