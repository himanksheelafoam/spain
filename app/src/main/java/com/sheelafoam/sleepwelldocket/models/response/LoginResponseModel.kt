package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.SerializedName
import com.sheelafoam.sleepwelldocket.models.Userdata

class LoginResponseModel {
    @SerializedName("status")
    val status: Int? =0

    @SerializedName("data")
    val data: Userdata? = null


}
