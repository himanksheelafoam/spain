package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DispatchListResponseModel:Serializable {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: DispatchDataModel? = null

}

class DispatchDataModel :Serializable{
    @SerializedName("message")
   var message: String? = null

    @SerializedName("count")
    var count: Int? = null

    @SerializedName("data")
    var data: List<DispatchListData>? = null
}

class DispatchListData:Serializable {
    @SerializedName("app_pl_number")
     var appPlNumber: String? = null

    @SerializedName("app_pl_date")
     var appPlDate: String? = null

    @SerializedName("c_p_id_name")
    var cPIdName: String? = null
    @SerializedName("createdby")
    var createdby: String? = null
}
