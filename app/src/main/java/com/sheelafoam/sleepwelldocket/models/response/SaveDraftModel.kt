package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SaveDraftModel :Serializable{
    @SerializedName("reference_number")
    var reference_number: String? = null
    @SerializedName("customer")
    var customer: UnitListData? = null
    @SerializedName("data")
    var data: List<Any>? = null

}
