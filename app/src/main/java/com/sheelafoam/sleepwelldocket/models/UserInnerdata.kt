package com.sheelafoam.sleepwelldocket.models

import com.google.gson.annotations.SerializedName


data class UserInnerdata(
    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("email")
    var email: String = "",

    @SerializedName("user_name")
    var userName: String = "",

    @SerializedName("password")
    var password: String = "",

    @SerializedName("has_password")
    var hasPassword: String = "",

    @SerializedName("permissions")
    var permissions: Any,

    @SerializedName("last_login")
    var lastLogin: Any,

    @SerializedName("first_name")
    var firstName: String = "",

    @SerializedName("last_name")
    var lastName: String = "",

    @SerializedName("phone")
    var phone: String = "",

    @SerializedName("address")
    var address: String = "",

    @SerializedName("image")
    var image: String ?=null,

    @SerializedName("user_id")
    var userId: String = "",

    @SerializedName("designation_id")
    var designationId: String = "",

    @SerializedName("employee_code")
    var employeeCode: String = "",

    @SerializedName("provisional_employee_code")
    var provisionalEmployeeCode: String = "",

    @SerializedName("parent_id")
    var parentId: String = "",

    @SerializedName("has_childs")
    var hasChilds: String = "",

    @SerializedName("remember_token")
    var rememberToken: String = "",

    @SerializedName("otp_auth")
    var otpAuth: String = "",

    @SerializedName("max_alw_session")
    var maxAlwSession: String = "",

    @SerializedName("zone_id")
    var zoneId: String = "",

    @SerializedName("department_id")
    var departmentId: String = "",

    @SerializedName("user_type_id")
    var userTypeId: String = "",

    @SerializedName("location")
    var location: String = "",

    @SerializedName("status")
    var status: String = "",

    @SerializedName("referred_by")
    var referredBy: String = "",

    @SerializedName("can_update")
    var canUpdate: String = "",

    @SerializedName("sw_certified")
    var swCertified: String = "",

    @SerializedName("created_at")
    var createdAt: String = "",

    @SerializedName("updated_at")
    var updatedAt: String = "",

    @SerializedName("is_synced")
    var isSynced: String = "",

    @SerializedName("synced_at")
    var syncedAt: String = "",

    @SerializedName("deleted_at")
    var deletedAt: String = "",

    @SerializedName("designation")
    var designation: String = "",

    @SerializedName("name")
    var name: String = "",

    @SerializedName("in_time")
    var inTime: String = "",

    @SerializedName("out_time")
    var outTime: String = "",

    @SerializedName("is_present")
    var isPresent: Boolean,

    @SerializedName("champion_status")
    var championStatus: Any,

    @SerializedName("parent_user_name")
    var parentUserName: String = "",

    @SerializedName("parent_user_id")
    var parentUserId: Int = 0,

    @SerializedName("is_update_status")
    var isUpdateStatus: Int,

    @SerializedName("roles")
    var roles: List<Object>

)

