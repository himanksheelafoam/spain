package com.sheelafoam.sleepwelldocket.models.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SaveDraftRequestModel : Serializable {
    @SerializedName("unit_id")
    var unit_id: Int? = 0

    @SerializedName("customer_id")
    var customer_id: Int? = 0

    @SerializedName("bundles")
    var bundles: ArrayList<String>? = null
    @SerializedName("reference_number")
    var reference_number: String? = null
}