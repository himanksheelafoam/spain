package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ValidateResponseModel :Serializable {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: ValidateDataModel? = null
}

class ValidateDataModel:Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: List<ValidateListData>? = null

}

class ValidateListData:Serializable {
    @SerializedName("bundle_number")
    var bundleNumber: String? = null

    @SerializedName("status")
    var status: Boolean? = null

    @SerializedName("remark")
    var remark: String? = null

}
