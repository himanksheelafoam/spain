package com.sheelafoam.sleepwelldocket.models.request

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginRequestModel : Serializable {

    @SerializedName("grant_type")
    var grant_type: String? = null

    @SerializedName("client_id")
    var client_id: String? = null

    @SerializedName("client_secret")
    var client_secret: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("scope")
    var scope: String? = null

    @SerializedName("password")
    var password: String? = null

    @SerializedName("firebase_id")
    var firebase_id: String? = null

    @SerializedName("device_name")
    var device_name: String? = null

    @SerializedName("device_address")
    var device_address: String? = null

    @SerializedName("os_version")
    var os_version: String? = null

    @SerializedName("app_version")
    var app_version: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null



}
