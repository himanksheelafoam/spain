package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DispatchDetailResponseModel :Serializable{
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
     var data: DispatchDetailDataModel? = null
}

class DispatchDetailDataModel :Serializable{
    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: List<DispatchDetailListModel>? = null
}

class DispatchDetailListModel:Serializable {
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("bundle_book_code")
    var bundleBookCode: String? = null

    @SerializedName("bundle_book_date")
    var bundleBookDate: String? = null

    @SerializedName("bundle_number")
    var bundleNumber: String? = null

    @SerializedName("receipt_id")
    var receiptId: String? = null

    @SerializedName("receipt_dtl_id")
    var receiptDtlId: String? = null

    @SerializedName("book_type")
    var bookType: String? = null

    @SerializedName("order_type")
    var orderType: String? = null

    @SerializedName("order_code")
     var orderCode: String? = null

    @SerializedName("order_number")
    var orderNumber: String? = null

    @SerializedName("order_date")
    var orderDate: String? = null

    @SerializedName("channel_partner_group")
    var channelPartnerGroup: String? = null

    @SerializedName("customer_id")
     var customerId: String? = null

    @SerializedName("c_p_id_name")
    var cPIdName: String? = null

    @SerializedName("unit_id")
    var unitId: String? = null

    @SerializedName("unit_code")
    var unitCode: String? = null

    @SerializedName("customer_category")
    var customerCategory: String? = null

    @SerializedName("main_product")
    var mainProduct: String? = null

    @SerializedName("sub_product")
   var subProduct: String? = null

    @SerializedName("product_description")
     var productDescription: String? = null

    @SerializedName("length")
    var length: String? = null

    @SerializedName("bredth")
    var bredth: String? = null

    @SerializedName("thick")
    var thick: String? = null

    @SerializedName("color")
    var color: String? = null

    @SerializedName("uom")
     var uom: String? = null

    @SerializedName("quantity_uom")
     var quantityUom: String? = null

    @SerializedName("quantity_nos")
   var quantityNos: String? = null

    @SerializedName("quantity_kgs")
    var quantityKgs: String? = null

    @SerializedName("m3c")
    var m3c: Double? = null

    @SerializedName("pl_link_id")
     var plLinkId: String? = null

    @SerializedName("is_synced")
    var isSynced: String? = null

    @SerializedName("app_pl_id")
    var appPlId: String? = null

    @SerializedName("app_pl_date")
     var appPlDate: String? = null

    @SerializedName("app_pl_number")
     var appPlNumber: String? = null

    @SerializedName("epl_id")
       var eplId: String? = null

    @SerializedName("epl_book_code")
    var eplBookCode: String? = null

    @SerializedName("epl_book_type")
     var eplBookType: String? = null

    @SerializedName("epl_book_date")
       var eplBookDate: String? = null

    @SerializedName("epl_book_number")
    var eplBookNumber: String? = null

    @SerializedName("is_delete")
     var isDelete: Any? = null

    @SerializedName("created_at")
    var createdAt: String? = null

    @SerializedName("updated_at")
     var updatedAt: String? = null

    @SerializedName("deleted_at")
      var deletedAt: String? = null
}
