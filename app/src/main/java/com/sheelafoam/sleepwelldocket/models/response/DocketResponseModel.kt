package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DocketResponseModel : Serializable {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: DocketDataModel? = null

}

class DocketDataModel : Serializable {
    @SerializedName("message")
    var message: String? = null

    @SerializedName("count")
    var count: Int? = null

    @SerializedName("data")
    var data: List<DocketListData>? = null
}

class DocketListData : Serializable {
    @SerializedName("app_pl_number")
    var app_pl_number: String? = null
}
