package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SaveDraftResponseModel :Serializable{
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: SaveDraftDataModel? = null

}

class SaveDraftDataModel:Serializable {
    @SerializedName("message")
    var message: String? = null

      @SerializedName("data")
    var data: SaveDraftModel? = null

}
