package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ScanDetailResponseModel:Serializable {
    @SerializedName("status")
    var status: Int? = null

    @SerializedName("data")
    var data: SubmitDataModel? = null
}

class SubmitDataModel :Serializable{
    @SerializedName("message")
    var message: String? = null

    @SerializedName("data")
    var data: List<ScanListData>? = null

}

class ScanListData:Serializable {
    @SerializedName("bundle_number")
    var bundleNumber: String? = null

    @SerializedName("status")
    var status: Boolean? = null

    @SerializedName("remark")
    var remark: String? = null
    @SerializedName("data")
    var bundledata: BundleListData? = null
}
