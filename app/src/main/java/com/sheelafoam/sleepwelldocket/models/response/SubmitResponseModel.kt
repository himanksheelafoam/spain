package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SubmitResponseModel :Serializable{
    @SerializedName("status")
     var status: Int? = null

    @SerializedName("data")
    var data: SubmitData? = null
    @SerializedName("errors")
    var errors: ErrorsModel? = null
}

class SubmitData :Serializable{
    @SerializedName("message")
     var message: String? = null

    @SerializedName("data")
    @Expose
    var data: SubmitInnerData? = null

}

class SubmitInnerData:Serializable {
    @SerializedName("app_pl_number")
    var appPlNumber: Int? = null

    @SerializedName("customer")
    var customer: CustomerModel? = null

    @SerializedName("date")
    var date: DateModel? = null
}

class DateModel :Serializable{
    @SerializedName("date")
    var date: String? = null

    @SerializedName("timezone_type")
    var timezoneType: Int? = null

    @SerializedName("timezone")
    var timezone: String? = null
}

class CustomerModel :Serializable{
    @SerializedName("name")
     var name: String? = null
}

class ErrorsModel:Serializable {
    @SerializedName("message")
    var message: String? = null
}
