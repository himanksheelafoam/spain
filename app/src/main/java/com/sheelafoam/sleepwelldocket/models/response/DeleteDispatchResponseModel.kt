package com.sheelafoam.sleepwelldocket.models.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DeleteDispatchResponseModel :Serializable{
    @SerializedName("status")
     var status: Int? = null

    @SerializedName("data")
    var data: DeleteDatamodel? = null
}

class DeleteDatamodel:Serializable {
    @SerializedName("message")
    var message: String? = null

}
