package com.sheelafoam.sleepwelldocket.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.models.response.ValidateListData
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import kotlinx.android.synthetic.main.bundle_cell.view.*


class BundleAdapter (private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<BundleViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BundleViewHolder {
        return BundleViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.bundle_cell, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: BundleViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is String) {
            holder.bundle.text=o.toString()
            holder.Sno.text= (position+1).toString()
            holder.remove.setOnClickListener(View.OnClickListener {
                mListener?.onItemClickListener(o,Constants.BUNDLE,"")
            })
        }else  if (o is ValidateListData) {
            if (o?.bundleNumber.toString()!=null) {
                holder.bundle.text = o?.bundleNumber.toString()
            }
            if (o?.status!=null){
                if (o?.status==true){
                    holder.remove.setText("VALIDATED")
                    holder.remove.setTextColor(Color.parseColor("#13E8A6"))
                    holder.error_layout.visibility=View.GONE
                }else{
                    holder.remove.setText("INVALID")
                    holder.remove.setTextColor(Color.parseColor("#E81365"))
                    holder.error_layout.visibility=View.VISIBLE
                    holder.errormessage.setText(o?.remark.toString())

                }
            }


        }

    }


}

class BundleViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val bundle: TextView = view.bundle
    val remove: TextView = view.remove
    val Sno: TextView = view.sno
    val errormessage: TextView = view.errormessage
    var main_layout: RelativeLayout =view.main_layout
    var error_layout: RelativeLayout =view.error_layout




}