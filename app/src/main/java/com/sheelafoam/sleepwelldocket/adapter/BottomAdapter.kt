package com.sheelafoam.sleepwelldocket.adapter

import android.net.wifi.rtt.CivicLocationKeys.LANGUAGE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import kotlinx.android.synthetic.main.common_list_dialog.view.*

class BottomAdapter(private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<BottomMenuViewHolder>() {

    /*, val clickListener: (CityResponseModel, Int) -> Unit*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BottomMenuViewHolder {
        return BottomMenuViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.common_list_dialog, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: BottomMenuViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is String) {
            val string: String = o as String

            if (string != null) {
                holder.name.text = string
            }
            holder?.name?.setOnClickListener {
                mListener?.onItemClickListener(string, Constants.LANGUAGE, "")
                // clickListener?.invoke(cityResponseModel, position)
            }
        }
    }


}

class BottomMenuViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.name

}