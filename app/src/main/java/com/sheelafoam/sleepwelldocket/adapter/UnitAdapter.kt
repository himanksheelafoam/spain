package com.sheelafoam.sleepwelldocket.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.models.response.CustomerListData
import com.sheelafoam.sleepwelldocket.models.response.DocketListData
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import kotlinx.android.synthetic.main.unit_cell.view.*

class UnitAdapter(private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<BottomSheetMenuViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BottomSheetMenuViewHolder {
        return BottomSheetMenuViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.unit_cell, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: BottomSheetMenuViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is UnitListData) {
            val unitListData: UnitListData = o as UnitListData

            if (unitListData.name != null) {
                holder.name.text = unitListData?.name
            }
            holder?.checkBox?.setOnClickListener {
                mListener?.onItemClickListener(unitListData, Constants.UNITS, "")

                holder.checkBox.setBackgroundResource(R.drawable.custom_selected_checkedbox)
                holder.dot.visibility=View.VISIBLE
                      // clickListener?.invoke(cityResponseModel, position)
            }
        }else  if (o is CustomerListData) {
            val customerListData: CustomerListData = o as CustomerListData

            if (customerListData.displayName != null) {
                holder.name.text = customerListData?.displayName
            }
            holder?.checkBox?.setOnClickListener {
                mListener?.onItemClickListener(customerListData, Constants.UNITS, "")

                holder.checkBox.setBackgroundResource(R.drawable.custom_selected_checkedbox)
                holder.dot.visibility=View.VISIBLE
                // clickListener?.invoke(cityResponseModel, position)
            }
        }
        else  if (o is DocketListData) {
            val docketListData: DocketListData = o as DocketListData

            if (docketListData.app_pl_number != null) {
                holder.name.text = docketListData?.app_pl_number
               holder.unithead.visibility=View.GONE
            }
            holder?.checkBox?.setOnClickListener {
                mListener?.onItemClickListener(docketListData, Constants.UNITS, "")

                holder.checkBox.setBackgroundResource(R.drawable.custom_selected_checkedbox)
                holder.dot.visibility=View.VISIBLE
                // clickListener?.invoke(cityResponseModel, position)
            }
        }
    }


}

class BottomSheetMenuViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.unitname
    val unithead: TextView = view.unithead
    val checkBox: RelativeLayout = view.checkbox_background
    val dot: ImageView = view.dot

}