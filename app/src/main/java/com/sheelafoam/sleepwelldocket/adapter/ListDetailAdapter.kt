package com.sheelafoam.sleepwelldocket.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R

import com.sheelafoam.sleepwelldocket.models.response.DispatchDetailListModel

import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import kotlinx.android.synthetic.main.dispatch_detail_cell.view.*


class ListDetailAdapter (private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<ListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.dispatch_detail_cell, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is DispatchDetailListModel) {
            val dispatchListData: DispatchDetailListModel = o as DispatchDetailListModel
            holder.bundleno.setText(dispatchListData?.bundleNumber?.toString())
            holder.orderno.setText(dispatchListData?.orderNumber?.toString())
            holder.itemname.setText(dispatchListData?.productDescription?.toString())
            holder.uom.setText(dispatchListData?.uom?.toString())
            holder.mc.setText(dispatchListData?.m3c?.toString())
            holder.qty.setText(dispatchListData?.quantityNos?.toString())

        }

    }


}

class ListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val itemcode: TextView = view.item_code
    val product_name: TextView = view.product_name
    val bundleno: TextView = view.bundleno
    val orderno: TextView = view.orderno
    val custref: TextView = view.custrefno
    val itemname: TextView = view.iten_name
    val uom: TextView = view.uom
    val mc: TextView = view.mc
    val qty: TextView = view.qty
    val rate: TextView = view.rate



}