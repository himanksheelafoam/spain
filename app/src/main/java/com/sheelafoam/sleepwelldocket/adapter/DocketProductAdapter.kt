package com.sheelafoam.sleepwelldocket.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.models.response.BundleListData
import com.sheelafoam.sleepwelldocket.models.response.DispatchDetailListModel
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import kotlinx.android.synthetic.main.add_product_popup.*

import kotlinx.android.synthetic.main.docket_product_cell.view.*


class DocketProductAdapter (private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<DocketProductViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocketProductViewHolder {
        return DocketProductViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.docket_product_cell, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: DocketProductViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is BundleListData) {
            val bundleListData: BundleListData = o as BundleListData

            if (bundleListData.mainProduct != null) {
                holder.product_name.text = bundleListData.mainProduct.toString()
            }
            if (bundleListData.subProduct != null) {
                holder.subproduct.text = bundleListData.subProduct
            }
            if (bundleListData.bundleNumber != null) {
                holder.bundleno.text = bundleListData.bundleNumber
            }
            if (bundleListData.orderNumber != null) {
                holder.orderno.text = bundleListData.orderNumber
            }
            if (bundleListData.productDescription != null) {
                holder.description.text = bundleListData.productDescription
            }
            if (bundleListData.color != null) {
                holder.color.text = bundleListData.color
            }
            if (bundleListData.uom != null) {
                holder.uom.text = bundleListData.uom
            }
            if (bundleListData.quantityNos != null) {
                holder.qty.text = bundleListData.quantityNos.toString()
            }
           /* if (bundleListData.quantityKgs != null) {
                holder.kg.text = bundleListData.quantityKgs
            }*/
            if (bundleListData.orderDate != null) {
               holder.date.text = bundleListData.orderDate
            }
            if (bundleListData?.m3c != null) {
               holder.mc.text = bundleListData?.m3c.toString()
            }
            if (bundleListData?.length != null) {
               holder.length.text = bundleListData?.length
            }
            if (bundleListData?.bredth != null) {
                holder.width.text = bundleListData?.bredth
            }
            if (bundleListData?.thick != null) {
                holder.thicness.text = bundleListData?.thick
            }
            holder.sno.text= (position+1).toString()+". "

            holder.remove.visibility=View.VISIBLE

            holder.remove.setOnClickListener(View.OnClickListener {
                mListener?.onItemClickListener(bundleListData,Constants.SCANDETAILS,"")
            })
        }else if (o is DispatchDetailListModel){
            val dispatchListData: DispatchDetailListModel = o as DispatchDetailListModel

            if (dispatchListData.mainProduct != null) {
                holder.product_name.text = dispatchListData.mainProduct.toString()
            }
            if (dispatchListData.subProduct != null) {
                holder.subproduct.text = dispatchListData.subProduct
            }
            if (dispatchListData.bundleNumber != null) {
                holder.bundleno.text = dispatchListData.bundleNumber
            }
            if (dispatchListData.orderNumber != null) {
                holder.orderno.text = dispatchListData.orderNumber
            }
            if (dispatchListData.productDescription != null) {
                holder.description.text = dispatchListData.productDescription
            }
            if (dispatchListData.color != null) {
                holder.color.text = dispatchListData.color
            }
            if (dispatchListData.uom != null) {
                holder.uom.text = dispatchListData.uom
            }
            if (dispatchListData.quantityNos != null) {
                holder.qty.text = dispatchListData.quantityNos
            }
          /*  if (dispatchListData.quantityKgs != null) {
                holder.kg.text = dispatchListData.quantityKgs
            }*/
            if (dispatchListData.orderDate != null) {
                holder.date.text = dispatchListData.orderDate
            }
            if (dispatchListData?.m3c != null) {
                holder.mc.text = dispatchListData?.m3c.toString()
            }
            if (dispatchListData?.length != null) {
                holder.length.text = dispatchListData?.length
            }
            if (dispatchListData?.bredth != null) {
                holder.width.text = dispatchListData?.bredth
            }
            if (dispatchListData?.thick != null) {
                holder.thicness.text = dispatchListData?.thick
            }
            holder.sno.text= (position+1).toString()+". "
            holder.remove.setOnClickListener(View.OnClickListener {
                mListener?.onItemClickListener(dispatchListData,Constants.SCANDETAILS,"")
            })


        }

    }


}

class DocketProductViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val subproduct: TextView = view.subproduct
    val product_name: TextView = view.product_name
    val bundleno: TextView = view.bundleno
    val orderno: TextView = view.orderno
    val description: TextView = view.description
    val color: TextView = view.color
    val uom: TextView = view.uom
    val mc: TextView = view.mc
    val qty: TextView = view.qty
    //val kg: TextView = view.kg
    val date: TextView = view.date
    val remove:TextView=view.remove
    val length: TextView = view.length
    val width: TextView = view.widthh
    val thicness: TextView = view.thickness
    val sno: TextView = view.sno




}