package com.sheelafoam.sleepwelldocket.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.sheelafoam.sleepwelldocket.R
import com.sheelafoam.sleepwelldocket.models.response.CartListModel
import com.sheelafoam.sleepwelldocket.models.response.DispatchListData
import com.sheelafoam.sleepwelldocket.models.response.DocketListData
import com.sheelafoam.sleepwelldocket.models.response.UnitListData
import com.sheelafoam.sleepwelldocket.utils.CommonItemClickListener
import com.sheelafoam.sleepwelldocket.utils.Constants
import com.sheelafoam.sleepwelldocket.utils.Utility
import kotlinx.android.synthetic.main.docketlistcell.view.*


class DocketListAdapter (private val dataField: List<*>, val mListener: CommonItemClickListener?) : RecyclerView.Adapter<DocketListViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DocketListViewHolder {
        return DocketListViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.docketlistcell, parent, false))
    }

    fun getData(): List<*> {
        return dataField
    }

    override fun getItemCount(): Int = dataField.size

    override fun onBindViewHolder(holder: DocketListViewHolder, position: Int) {
        val o: Any = dataField.get(position)!!
        if (o is DispatchListData) {
            val dispatchListData: DispatchListData = o as DispatchListData
            if(o?.cPIdName!=null){
                holder.name.setText(o?.cPIdName)
            }
            if(o?.appPlNumber!=null){
                holder.number.setText("#"+o?.appPlNumber)
            }
            if(o?.createdby!=null){
                holder.createdby.setText("Created By :"+o?.createdby)
            }
            if(o?.appPlDate!=null){
                holder.date.setText(Utility.convertDateFormat(o?.appPlDate,"dd-MM-yyyy"))
            }
            holder.main_layout.setOnClickListener(View.OnClickListener {
                mListener?.onItemClickListener(dispatchListData,Constants.DISPATCHLIST,"")
            })
        }else  if (o is CartListModel) {
            val cartListModel: CartListModel = o as CartListModel
            if(o?.customer!=null){
                holder.name.setText(o?.customer)
            }
            if(o?.referenceNumber!=null){
                holder.number.setText(o?.referenceNumber)
            }
            if(o?.createdby!=null){
                holder.createdby.setText("Created By :"+o?.createdby)
            }
            if(o?.date!=null && !o?.date.equals("")){
                holder.date.setText(Utility.convertDateFormat(o?.date,"dd-MM-yyyy"))
            }
            holder.main_layout.setOnClickListener(View.OnClickListener {
                mListener?.onItemClickListener(cartListModel,Constants.DISPATCHLIST,"")
            })
        }

    }


}

class DocketListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    val name: TextView = view.name
    val date: TextView = view.date
    val number: TextView = view.number
    val createdby: TextView = view.createdby
    var main_layout:RelativeLayout=view.main_layout

}