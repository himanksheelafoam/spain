package com.sheelafoam.sleepwelldocket.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import pl.charmas.android.reactivelocation2.ReactiveLocationProvider;
import pl.charmas.android.reactivelocation2.ReactiveLocationProviderConfiguration;

public class LocationUpdateManager  {
    private Context context;
    private Disposable updatableLocationDisposable;
    private Observable<Location> locationUpdatesObservable;
    private ReactiveLocationProvider locationProvider;
    public final static int REQUEST_CHECK_SETTINGS = 25;
    private Activity activity;
    public CommonItemClickListener commonItemClickListener;

    public LocationUpdateManager(Context ctx, Activity activity, CommonItemClickListener commonItemClickListener) {
        context = ctx;
        this.activity = activity;
        this.commonItemClickListener = commonItemClickListener;
        onCreate();
    }

    public void onCreate() {
        locationProvider = new ReactiveLocationProvider(context, ReactiveLocationProviderConfiguration
                .builder()
                .setRetryOnConnectionSuspended(true)
                .build()
        );
        final LocationRequest locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(30000)
                .setNumUpdates(1);


        locationUpdatesObservable = locationProvider
                .checkLocationSettings(
                        new LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest)
                                .setAlwaysShow(true)  //Refrence: http://stackoverflow.com/questions/29824408/google-play-services-locationservices-api-new-option-never
                                .build()
                )
                .doOnNext(new Consumer<LocationSettingsResult>() {
                    @Override
                    public void accept(LocationSettingsResult locationSettingsResult) {
                        Status status = locationSettingsResult.getStatus();
                        if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                            try {
                                Log.e("manager", "locationnnn");
                                status.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException th) {
                                Log.e("manager", "Error opening settings activity.", th);
                            }
                        }
                    }
                })
                .flatMap(new Function<LocationSettingsResult, Observable<Location>>() {
                    @Override
                    public Observable<Location> apply(LocationSettingsResult locationSettingsResult) {
                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return null;
                        }
                        return locationProvider.getUpdatedLocation(locationRequest);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
        onLocationPermissionGranted();
    }


    public void onLocationPermissionGranted() {

        updatableLocationDisposable = locationUpdatesObservable
//                .map(new LocationToStringFunc())
                .subscribe(
                        new Consumer<Location>() {
                            @Override
                            public void accept(Location location) throws Exception {
                                location.getLatitude();

                                Log.e("@@Location->lat> ", "" + location.getLatitude());
                                Log.e("@@Location->long> ", "" + location.getLongitude());

                                commonItemClickListener.onItemClickListener(location, "", "");

                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) throws Exception {

                            }
                        });
    }


    public void stopLocationUpdate() {
        if (updatableLocationDisposable != null && !updatableLocationDisposable.isDisposed()) {
            updatableLocationDisposable.dispose();

        }
    }

}