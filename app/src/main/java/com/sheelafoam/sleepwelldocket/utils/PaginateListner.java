package com.sheelafoam.sleepwelldocket.utils;

import androidx.recyclerview.widget.RecyclerView;

public abstract class PaginateListner extends RecyclerView.OnScrollListener {

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
            if (!isLoading() && !isLastPage() && !recyclerView.canScrollVertically(1)) {
                loadMoreItems();
            }
        }
    }

    protected abstract void loadMoreItems();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();
}
