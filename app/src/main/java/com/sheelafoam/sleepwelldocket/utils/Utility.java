package com.sheelafoam.sleepwelldocket.utils;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.sheelafoam.sleepwelldocket.R;
import com.sheelafoam.sleepwelldocket.activities.BaseActivity;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static androidx.core.content.ContextCompat.checkSelfPermission;

public class Utility {
    /**
     * show toast message to user
     *
     * @param context
     * @param message
     */



    /**
     * it shows log for test user
     *
     * @param tag
     * @param string
     */
    public static void showLog(String tag, String string) {
        try {
            Log.e(tag, "@" + string);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * show alert dialog to user inform
     *
     * @param context
     * @param message
     */

    /**
     * This method is used for add fragment
     *
     * @param context
     * @param fragment
     */
    public static void addFragment(Context context, Fragment fragment, Bundle bundle) {
        FragmentManager fragmentManager = ((BaseActivity) context).getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        ft.add(R.id.frame_layout, fragment, fragment.getClass().getName()).addToBackStack(fragment.getClass().getName());
//        ft.replace(R.id.frame_layout, fragment).addToBackStack(fragment.getClass().getName());
        ft.commit();
    }

    /**
     * This method is used for check null value
     *
     * @param value
     * @return
     */
    public static boolean checkEmptyString(String value) {
        return value != null && !value.equalsIgnoreCase("");
    }


    public static void showToast(Activity context, String message) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast,
                (ViewGroup) context.findViewById(R.id.custom_toast_container));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.TOP|Gravity.LEFT, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    public static void errorToast(Activity context, String message) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_error_toast,
                (ViewGroup) context.findViewById(R.id.custom_toast_container));

        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(context);
//        toast.setGravity(Gravity.BOTTOM|Gravity.LEFT, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }


    public static boolean checkAndRequestGalleryPermissions(Activity activity) {

        int permissionStorageWrite = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionStorageRead = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionStorageWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionStorageRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermission(activity, listPermissionsNeeded,
                    2);
            return false;
        }
        return true;
    }
    public static boolean checkAndRequestCameraPermissions(Activity activity) {
        int permissionCamera = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.CAMERA);
        int permissionStorageWrite = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionStorageRead = ContextCompat.checkSelfPermission(activity,
                android.Manifest.permission.READ_EXTERNAL_STORAGE);

        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCamera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (permissionStorageWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionStorageRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            requestPermission(activity, listPermissionsNeeded,
                    1);
            return false;
        }
        return true;
    }



    public static void requestPermission(Activity activity, List<String> listPermissionsNeeded, int PERMISSION_REQUEST_CODE) {
        ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSION_REQUEST_CODE);
    }


    public static boolean checkLocationPermission(Activity activity) {

        int permissionStorageWrite = checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionStorageRead = checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (permissionStorageWrite != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionStorageRead != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            activity.requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 11);

            return false;
        }
        return true;
    }
    public static String convertDateFormat(String inputFormat, String outPutFormat) {

        List<SimpleDateFormat> knownPatterns = new ArrayList<SimpleDateFormat>();
        knownPatterns.add(new SimpleDateFormat("dd/MM/yyyy"));
        knownPatterns.add(new SimpleDateFormat("MM-dd-yyyy"));
        knownPatterns.add(new SimpleDateFormat("dd MMM yyyy"));
        knownPatterns.add(new SimpleDateFormat("dd MMM yyyy hh:mm:ss"));
        knownPatterns.add(new SimpleDateFormat("dd MMM yyyy hh:mm:ss"));
        knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd"));
        knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd HH:MM:SS"));
        knownPatterns.add(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss"));
        DateFormat outputFormat = new SimpleDateFormat(outPutFormat);
        Date date = null;
        for (int i = 0; i < knownPatterns.size(); i++) {
            try {
                date = knownPatterns.get(i).parse(inputFormat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return outputFormat.format(date);
    }




    public static void replaceFragment(Context context, Fragment fragment, Bundle bundle) {

        try {
            String backStackName = fragment.getClass().getName();
            FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();
            if (bundle != null) {
                fragment.setArguments(bundle);
            }
            FragmentTransaction transaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
//            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.replace(R.id.frame_layout, fragment)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }









    public static String toCamelCase(String s) {
        if (s.length() == 0) {
            return s;
        }
        String[] parts = s.split(" ");
        String camelCaseString = "";
        for (String part : parts) {
            camelCaseString = camelCaseString + toProperCase(part) + " ";
        }
        return camelCaseString;
    }

    public static String toProperCase(String s) {
        return s.substring(0, 1).toUpperCase() +
                s.substring(1).toLowerCase();
    }

    public static boolean isValidEmail(CharSequence target) {
        return (Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean onlyContainsNumbers(String text) {
        try {
            Long.parseLong(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }

    }



    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model) + "(Android)";
        } else {
            return capitalize(manufacturer) + " " + model + "(Android)";
        }
    }
    public static String getDeviceOs() {

        StringBuilder builder = new StringBuilder();
        try {

            builder.append("android : ").append(Build.VERSION.RELEASE);

            Field[] fields = Build.VERSION_CODES.class.getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                int fieldValue = -1;

                try {
                    fieldValue = field.getInt(new Object());
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                if (fieldValue == Build.VERSION.SDK_INT) {
                    builder.append(" : ").append(fieldName).append(" : ");
                    builder.append("sdk=").append(fieldValue);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("DEVICE_OS", builder.toString());
        return builder.toString();
    }


    public static String getAppVersion(Context context) {

        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String version = pInfo.versionName;
            return version;
        } catch (
                PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void expand(final View v, int duration, int targetHeight) {

        int prevHeight = v.getHeight();

        v.setVisibility(View.VISIBLE);
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }

    public static void collapse(final View v, int duration, int targetHeight) {
        int prevHeight = v.getHeight();
        ValueAnimator valueAnimator = ValueAnimator.ofInt(prevHeight, targetHeight);
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                v.getLayoutParams().height = (int) animation.getAnimatedValue();
                v.requestLayout();
            }
        });
        valueAnimator.setInterpolator(new DecelerateInterpolator());
        valueAnimator.setDuration(duration);
        valueAnimator.start();
    }




    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    public static Point getLocationOnScreen(View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        return new Point(location[0], location[1]);
    }

    public static void changeImageViewBgColor(GradientDrawable drawable, String colorCode, View view) {
        try {
            drawable.setColor(Color.parseColor(colorCode));
            view.setBackground(drawable);
        } catch (Exception e) {
        }
    }

    public static int getScreenHeight(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
//        int width = displayMetrics.widthPixels;
        return height;
    }




    /**
     * This function returns map icon from drawable icon
     *
     * @param context
     * @param vectorResId
     * @return
     */

}
