package com.sheelafoam.sleepwelldocket.utils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class Constants {
    public static final String LOGIN="login";
    public static final String UNITS="units";
    public static final String UNIT_ID="unit_id";
    public static final String CUSTOMERS="customers";
    public static final String CUSTOMER_ID="customer_id";
     public static final String SCANDETAILS="scan_details";
    public static final String SUBMIT="Submit";
    public static final String BUNDLE="bundle";
    public static final String DISPATCHLIST="dispatchlist";
    public static final String DISPATCHDETAIL="dispatch_detail";
    public static final String DOCKET="docket";
     public static final String LANGUAGE="language";
    public static final String DELETEDISPATCH="delete_dispatch";
    public static final String ACTIONTYPE="acction_type";
     public static final String SAVEDRAFT="savedraft";
    public static final String CART="cart";
    public static final String CARTDETAIL="cart_detail";
    public static final String FROM="from";
   public static final String REFERENCE="reference";
}
