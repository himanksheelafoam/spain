package com.sheelafoam.sleepwelldocket.utils;

public interface CommonItemClickListener {
    void onItemClickListener(Object o, String isFrom,String position);

}
