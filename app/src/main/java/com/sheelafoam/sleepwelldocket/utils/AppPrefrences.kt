package com.sheelafoam.sleepwelldocket.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.sheelafoam.sleepwelldocket.models.Userdata
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences.Data.editor
import com.sheelafoam.sleepwelldocket.utils.AppPrefrences.Data.sSharedPreferences



class AppPrefrences  {@SuppressLint("StaticFieldLeak")
object Data {
    lateinit var sSharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    private var _context: Context? = null
    private val PREF_NAME = "SalusPrefrence"
    lateinit var uniqInstance: AppPrefrences

    @JvmStatic
    fun getInstance(context: Context): AppPrefrences {
        _context = context
        sSharedPreferences = _context!!.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        editor = sSharedPreferences.edit()
        uniqInstance = AppPrefrences()

        return uniqInstance
    }
}

fun clearPreference() {
    editor.clear()
    editor.commit()
}


fun   setAccessToken(accessToken: String?) {
    editor.putString("accessToken", accessToken)
    editor.commit()
}

fun getAccessToken(): String? {
    return sSharedPreferences.getString("accessToken", "")
}
    fun   setLanguage(language: String?) {
        editor.putString("language", language)
        editor.commit()
    }

    fun getLanguage(): String? {
        return sSharedPreferences.getString("language", "")
    }



fun setFirebaseToken(token: String) {
    editor.putString("fbToken", token)
    editor.commit()
}

fun getFirebaseToken(): String? {
    return sSharedPreferences.getString("fbToken", "")
}


open fun saveUserModel(userModel: Userdata?, key: String?, context: Context?
) {
    val prefs = PreferenceManager.getDefaultSharedPreferences(context)
    val editor = prefs.edit()
    val gson = Gson()
    val json = gson.toJson(userModel)
    editor.putString(key, json)
    editor.commit()
}


open fun getUserModel(context: Context?, key: String?): Userdata? {
    val mprefs = PreferenceManager.getDefaultSharedPreferences(context)
    val gson = Gson()
    val json = mprefs.getString(key, "")
    return gson.fromJson(json, Userdata::class.java)
}


}