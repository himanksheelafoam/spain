package com.sheelafoam.sleepwelldocket.utils

import android.app.Dialog
import android.content.Context
import android.view.Window

class CustomDialog(context: Context?, layoutId: Int) :
    Dialog(context!!) {
    init {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(layoutId)
    }
}