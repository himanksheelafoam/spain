package com.sheelafoam.sleepwelldocket.utils

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class SearchRequest : Serializable {

    @SerializedName("content")
    var search: String? = null

    @SerializedName("userId")
    var userId: String? = null


}
